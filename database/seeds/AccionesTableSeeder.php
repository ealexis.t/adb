<?php

use Illuminate\Database\Seeder;

class AccionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Accion::class)->create([
            'acciones' => 'Promover la asistencia a preescolar de niños entre 3 y 5 años.',
            'idUsers' => '6',
            'idCarencia' => '1',
            'idEntidad' => '1',

            ]);

        factory(App\Accion::class)->create([
            'acciones' => 'Certificación de los nacidos hasta 1981 que cuentan con 5° o 6° de primaria.',
            'idUsers' => '6',
            'idCarencia' => '1',
            'idEntidad' => '1',

            ]);

        factory(App\Accion::class)->create([
            'acciones' => 'Certificación de adultos mayores en primaria.',
            'idUsers' => '6',
            'idCarencia' => '1',
            'idEntidad' => '1',

            ]);

        factory(App\Accion::class)->create([
            'acciones' => 'Certificación de los nacidos a partir 1981 que cuentan con 2° o 3° de secundaria.',
            'idUsers' => '6',
            'idCarencia' => '1',
            'idEntidad' => '1',

            ]);

    }
}