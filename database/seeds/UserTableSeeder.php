<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
            'username'  => 'manuel',
            'password'  => bcrypt('adminmanuel'),
            'email'     => 'correo@gmail.com',
            'nombre'     => 'Admin',
            'apPaterno' => 'Admin',
            'apMaterno'  => 'Admin',
            'rol' => '1',
            ]);

        factory(App\User::class)->create([
            'username'   => 'alexis',
            'password'   => bcrypt('dr4c4rys'),
            'email'      => 'ealexis.t@gmail.com',
            'nombre'     => 'alexis',
            'apPaterno' => 'Tapia',
            'apMaterno'  => 'Hipolito',
            'rol' => '1',
            ]);

        factory(App\User::class)->create([
            'username'  => 'admin',
            'password'  => bcrypt('admin'),
            'email'     => 'correo@gmail.com',
            'nombre'     => 'Admin',
            'apPaterno' => 'Admin',
            'apMaterno'  => 'Admin',
            'rol' => '1',
            ]);

        factory(App\User::class)->create([
            'username'   => 'editor',
            'password'   => bcrypt('editor'),
            'email'      => 'correo@gmail.com',
            'nombre'     => 'Editor',
            'apPaterno'  => 'Editor',
            'apMaterno'  => 'Editor',
            'rol' => '2',
            ]);

        factory(App\User::class)->create([
            'username'   => 'user',
            'password'   => bcrypt('user'),
            'email'      => 'correo@gmail.com',
            'nombre'     => 'User',
            'apPaterno'  => 'User',
            'apMaterno'  => 'User',
            'rol' => '3',
            ]);
    }
}
