<?php


 ?>
@extends('layouts.login')

@section('content')
<div class="container logincontainer">
    <div class="row logincontra">
        <div class="col-md-8">
            <div class="">
                <div class="col-md-10 col-sm-10">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">


                            <div class="col-md-10 col-sm-10">
                                <label class="col-md-10 col-sm-10">Usuario</label>
                                <br>
                                <input type="text" class="form-control" name="username" value="{{ old('username') }}">

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">


                            <div class="col-md-10 col-sm-10">
                                <label class="col-md-10 col-sm-10 loginletra">Contraseña</label>
                                <br>
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-md-offset-6 col-sm-offset-6">
                                <button type="submit" class="col-md-12 col-sm-12 btn btn-primary loginenviar">Entrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
