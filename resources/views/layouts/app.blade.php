<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>SEDESOL CONTIGO</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">


<!-- BEGIN GLOBAL MANDATORY STYLES -->

<link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">

<!-- Global styles END -->

<!-- Page level plugin styles BEGIN -->
<link href="/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
<!-- Page level plugin styles END -->


<link href="/assets/global/css/components.css" rel="stylesheet">
<link href="/assets/frontend/onepage/css/style.css" rel="stylesheet">
<link href="/assets/frontend/onepage/css/style-responsive.css" rel="stylesheet">
<link href="/assets/frontend/onepage/css/themes/red.css" rel="stylesheet" id="style-color">
<link href="/assets/frontend/onepage/css/custom.css" rel="stylesheet">
<link href="/assets/css/style.css" rel="stylesheet">
@yield('style')

{{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

@yield('css-extras')
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
</head>

<body class="page-header-fixed page-quick-sidebar-over-content">
@if(Session::has('message'))
    <div class="modal-success">
        <div class="message col-md-4 col-md-offset-4">
            <a class="pull-right" href="{{ URL('home') }}">Cerrar</a>
            <div class="text-center">
                <img class="icon-mensaje" src="{{ asset('/assets/image/ok.png') }}" alt="" height="100px" width="100px">
            </div>
            <hr style="border:solid #444 2px;">
            <p class="text-center">
                {{ Session::get('message') }}
            </p>
        </div>
    </div>
    <script>
        function timer(){
            $(".modal-success").hide('fast');
            setInterval(redirect, 2000);
        }
        function redirect(){
            $("modal-success").css('display', 'none');
        }
        setInterval(timer, 1000);
    </script>
@endif
@if(Session::has('message-error'))
    <div class="modal-success">
        <div class="message col-md-4 col-md-offset-4">
            <a class="pull-right" href="{{ URL('home') }}">Cerrar</a>
            <div class="text-center">
                <img class="icon-mensaje" src="{{ asset('/assets/image/error.png') }}" alt="" height="100px" width="100px">
            </div>
            <hr style="border:solid #444 2px;">
            <p class="text-center">
                {{ Session::get('message-error') }}
            </p>
        </div>
    </div>
    <script>
        function timer(){
            $(".modal-success").hide('fast');
            setInterval(redirect, 3000);
        }
        function redirect(){
            $("modal-success").css('display', 'none');
        }
        setInterval(timer, 2000);
    </script>
@endif
<!-- BEGIN HEADER -->
<!-- END HEADER -->
<div class="clearfix">
</div>

<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="container">
            @yield('content')
        </div>
    </div>
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
    <script src="/assets/js/app.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

<!-- Core plugins END (For ALL pages) -->

<!-- BEGIN RevolutionSlider -->

<!-- END RevolutionSlider -->

<!-- Core plugins BEGIN (required only for current page) -->
<script src="/assets/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
<script src="/assets/global/plugins/jquery.easing.js"></script>
<script src="/assets/global/plugins/jquery.parallax.js"></script>
<script src="/assets/global/plugins/jquery.scrollTo.min.js"></script>
<script src="/assets/frontend/onepage/scripts/jquery.nav.js"></script>
<!-- Core plugins END (required only for current page) -->

<!-- Global js BEGIN -->
<script src="/assets/frontend/onepage/scripts/layout.js" type="text/javascript"></script>

<script>
    $(document).ready(function() {
      Layout.init();
    });
</script>

@yield('js-extras')
</body>
</html>