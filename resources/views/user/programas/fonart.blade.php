@extends('layouts.programas')
    @include('partials/programas',array())
@section('content')
<div class="container">
    <div class="row head">
        <div class="col-md-12 ">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <img src="{!! asset('/assets/image/programasPng/fonart.png') !!}" width="150px" height="150px">
            </div>
            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 text-right title_ins" >
            <h1>FONART</h1>
                <p>
                Fondo Nacional para el Fomento de las Artesanías
                </p>
            </div>
        </div>
    </div>
</div>
<div class="row blank">
  <!-- TABS -->
  <div class="col-md-12 tab-style-1">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
      <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
    </ul>
    <div class="tab-content">
      <div id="tab-1" class="tab-pane row fade active in">
        <div class="col-md-12 text-left">
          <p class="margin-bottom-10">
            Mejorar las capacidades productivas y comerciales
            de las y los artesanos con ingresos por debajo de
            la línea de bienestar, a través de apoyos y
            desarrollo de proyectos productivos.
          </p>
          <div class="text-center">
            @include('partials.imgProgramas.fonart.fonart1',array())
          </div>
        </div>
      </div>
      <div id="tab-2" class="tab-pane row fade">
        <div class="col-md-12 text-center">
          <p>
            Artesanas y artesanos mexicanos cuyo ingreso
            está por debajo de la línea de bienestar.
          </p>
          <div class="text-center">
          @include('partials.imgProgramas.fonart.fonart2',array())
          </div>

        </div>
      </div>
      <div id="tab-3" class="tab-pane fade">

        <div class="col-md-12 text-left">
            <h4>Capacitación Integral y Asistencia Técnica</h4>
          <ul class="text-left listado">
            <li><p>
            Mejora la condición productiva de las y los
            artesanos, a través de la transmisión de
            conocimientos.
            </p></li>
          </ul>
          <h4>Capacitación Integral</h4>
            <ul class="text-left listado">
            <li><p>
              Cubre los gastos hasta por un monto de 15 mil
            pesos por beneficiario (grupos de mínimo 15
            artesanos) para capacitaciones con una duración de
            hasta 12 meses.
            </p></li>
          </ul>
            
          <h4>Asistencia Técnica</h4>
            <ul class="text-left listado">
            <li>            <p>
              Apoya con un monto de 6 mil pesos por
              artesano para asistencias de hasta 6 meses.
            </p></li>
          </ul>

            <h4>Apoyos para Impulsar la Producción</h4>
            <ul class="text-left listado">
            <li><p>
            <strong> Apoyos Individuales</strong>
            Otorga apoyos hasta por 15 mil pesos de
            manera anual.
            </p></li>
            <li><p>
            <strong>Apoyos Grupales</strong>
            Apoya a grupos de 5 a 15 artesanos, el
            monto por artesano no debe rebasar los 15
            mil pesos, ni el 90% del costo total del
            proyecto.
            <br>
            Otorga un monto de hasta 225 mil pesos,
            con previo acuerdo del COVAPA; Si el grupo
            rebasa el límite máximo de artesanos, la
            cantidad señalada será distribuida
            equitativamente entre el total de
            participantes.
            </p></li>
          </ul>
          <h4>Apoyos para Impulsar la Comercialización</h4>
            <ul class="text-left listado">
            <li><p>
              <strong>Acopio de Artesanías</strong>
              Financiamiento de hasta 15 mil pesos al año
              para impulsar la comercialización de su
              producción.
            </p></li>
            <li><p>
              <strong>Demanda comercial específica</strong>
              Atiende las necesidades concretas y
              excepcionales del mercado, otorgando un
              apoyo de hasta 225 mil pesos por solicitud,
              previa autorización del COVAPA.
            </p></li>
            </ul>

          <h4>Apoyos para la promoción artesanal en Ferias y Exposiciones</h4>
            <ul class="text-left listado">
            <li><p>
              <strong>Apoyos Individuales</strong>
              Otorga apoyos hasta por 15 mil pesos de manera anual.
            </p></li>
            <li><p>
              <strong>Apoyos Grupales</strong>
              Apoya a grupos de 5 a 15 artesanos, el
              monto por artesano no debe rebasar los 15
              mil pesos, ni el 70% de la cotización
              presentada dentro del proyecto para la
              promoción artesanal.
              <br>
              Otorga un monto de hasta 225 mil pesos,
              con previo acuerdo del COVAPA, si el grupo
              rebasa el límite máximo de artesanos, la
              cantidad señalada será distribuida
              equitativamente entre el total de
              participantes.
            </p></li>

            </ul>
            <h4>Concursos de Arte Popular</h4>
            
            <ul class="text-left listado">
            <li>            <p>
              Premia a cualquier artesano del país que se
              distinga por la preservación, rescate o
              innovación de las artesanías, sin importar
              su nivel de ingresos.
            </p></li>
            </ul>
            


            <h4>Apoyos para la Salud Ocupacional</h4>
            <ul class="text-left listado">
            <li><p>
              Otorga apoyos hasta por 15 mil pesos de
              manera anual por artesano para la compra
              de prendas, accesorios y equipo o para
              realizar talleres referentes a Salud
              Ocupacional.
            </p></li>
            <li><p>
              Busca reducir la incidencia de las
              enfermedades y accidentes derivados de la
              actividad artesanal, así como fomentar un
              estilo de vida y ambiente de trabajo
              saludable.
            </p></li>

            
            </ul>
            <h4>Acciones para el Desarrollo de Espacios Artesanales en Destinos Turísticos</h4>
            <ul class="text-left listado">
            <li><p>
              Beneficia a grupos de artesanas y/o
              artesanos para mejorar sus talleres o
              cualquier espacio artesanal que se
              encuentre localizado en algún destino
              turístico, ruta artesanal turística o pueblo
              mágico.
            </p></li>
            <li><p>
              Otorga un monto de hasta 70 mil pesos al año.
            </p></li>

            </ul>

            <h4>Apoyos para Proyectos Artesanales Estratégicos</h4>
            <ul class="text-left listado">
            <li><p>
              Apoya de manera integral o parcial a grupos
              de artesanos asociados que tienen alto
              potencial en el mercado y representan un
              capital cultural.
            </p></li>
            </ul>
            <div class="text-center">
              @include('partials.imgProgramas.fonart.fonart3',array())
            </div>
      </div>
      </div>
       <div id="tab-4" class="tab-pane row fade">

        <div class="col-md-12 text-justify">
          <ul class="text-left listado">
            <li>
                <p>
                    Entregar la Solicitud de Apoyo, la cual puede obtenerse en las oficinas de FONART. Entregar una copia y mostrar el original vigente para cotejo de una identificación la cual puede ser:
                </p>
                <ul class="text-left listado">
                  <li>
                    <p>
                      Credencial para votar vigente
                    </p>
                  </li>
                  <li>
                    <p>
                      Cartilla del Servicio Militar Nacional
                    </p>
                  </li>
                  <li>
                    <p>
                      Pasaporte
                    </p>
                  </li>
                  <li>
                    <p>
                      Clave Única de Registro de Población (CURP)
                    </p>
                  </li>
                </ul>
                <br><br>
                  <li>
                    <p>
                      Manifestar en forma escrita o electrónica cual es el interés en recibir el apoyo.
                    </p>
                  </li>
                  <li>
                    <p>
                      Manifestar bajo protesta de decir verdad que durante el presente año no se han recibido apoyos de otros programas federales para los mismos conceptos que se recibirán del FONART; así como no tener adeudos por
                      gastos no comprobados con el FONART.
                    </p>
                  </li>
                  <li>
                    <p>
                      En ningún caso se otorgará apoyo a asociaciones civiles, instituciones educativas u otros organismos con los que no se cuente con convenio de colaboración firmado.
                    </p>
                  </li>
                  <li>
                  <p>
                    Para los Concursos de Arte Popular, cumplir con todos los requisitos de cada Convocatoria y entregar la Ficha de Registro de Concursos.
                  </p>
                  </li>
            </li>
          </ul>
            <div class="text-center">
              @include('partials.imgProgramas.fonart.fonart4',array())
            </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END TABS -->
</div>

@endsection
@section('modals')
@endsection
@section('js-extras')
@endsection