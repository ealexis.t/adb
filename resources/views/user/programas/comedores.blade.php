    @extends('layouts.programas')
    @include('partials/programas',array())
    @section('content')

    <div class="container">
        <div class="row head">
            <div class="col-md-12 ">
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <img src="/assets/image/programasPng/comedores.png" width="150px" height="150px">
                </div>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 text-right title_ins" >
                <h1>COMEDORES COMUNITARIOS</h1>

                </div>
            </div>
        </div>
    </div>
    <div class="row blank">
      <!-- TABS -->
      <div class="col-md-12 tab-style-1">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
          <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
          <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
            <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
        </ul>
        <div class="tab-content">
          <div id="tab-1" class="tab-pane row fade active in">
            <div class="col-md-12 text-left">
              <p class="margin-bottom-10">
                Contribuir a fortalecer el cumplimiento efectivo de
                los derechos sociales que potencien las
                capacidades de las personas en situación de
                pobreza, a través de acciones que incidan
                positivamente en la alimentación mediante la
                instalación y operación de Comedores
                Comunitarios.
              </p>
              <div class="text-center">
                @include('partials.imgProgramas.comedores.comedor1',array())
              </div>

            </div>
          </div>
          <div id="tab-2" class="tab-pane row fade">
            <div class="col-md-12 text-left">
              <p>
                A Zonas de Atención Prioritaria (ZAP), mediante la
                instalación y operación de comedores constituidos
                a través de la participación social, (Comités
                Comunitarios de la Cruzada Contra el Hambre).
                De acuerdo con la atribución del Comité
                Comunitario se da prioridad en atención a los
                siguientes grupos de población:
              </p>
              <ul class="text-left listado">
                <li><p>
                  Niñas y niños de 0 a 11 años de edad
                </p></li>
                <li><p>
                  Estudiantes adolescentes de 12 a 19 años
                </p></li>
                <li><p>
                  Mujeres embarazadas y en periodo de lactancia
                </p></li>
                <li><p>
                  Personas con alguna discapacidad
                </p></li>
                <li><p>
                  Personas adultas mayores de 65 años y más
                </p></li>
                <li><p>
                  Población que sufra contingencias o
                  emergencias que el gobierno federal haya
                  declarado como zonas de desastre
                </p></li>
                <li><p>
                  Personas en situación de vulnerabilidad
                  (mujeres violentadas, personas
                  desempleadas o ingreso insuficiente,
                  personas migrantes, personas en situación
                  de calle, entre otras personas con carencia
                  por acceso a la alimentación).
                </p></li>
              </ul>

                <div class="text-center">
                  @include('partials.imgProgramas.comedores.comedor2',array())
                </div>
            </div>
          </div>
          <div id="tab-3" class="tab-pane fade">
            <div class="col-md-12 text-left">
                <ol class="text-left listado">
                  <li><p>
                    Programa Comedores Comunitarios:
                    </p>
                    <ul class="text-left listado">
                      <li><p>
                        Proporciona el equipamiento de cocina por
                        única vez en la instalación del Comedor.
                      </p></li>
                      <li><p>
                        Proporciona de forma mensual los alimentos
                        no perecederos (suministro,
                        almacenamiento y distribución hasta la
                        localidad).
                      </p></li>
                      <li><p>
                        Otorga capacitaciones por parte de
                        diferentes instituciones de la Administración
                        Pública Federal y otros actores sociales.
                      </p></li>
                    </ul>
                  </li>
                  <li>
                    <p>Comités Comunitarios:</p>
                    <ul class="listado">
                      <li><p>
                        Instala comedores en el espacio físico y
                        social del quehacer comunitario para la
                        preparación y el consumo de alimentos
                        entre la población.
                      </p></li>
                      <li><p>
                        Crea lugares incluyentes donde se fomenta
                        la relación familiar y social, la sana
                        convivencia, la comunicación, el encuentro,
                        y la participación, como atributos básicos de
                        la cohesión social.
                      </p></li>
                      <li><p>
                        Permite detonar procesos relacionados con
                        acciones que fortalezcan la cohesión social
                        entre los miembros de la comunidad.
                      </p></li>
                    </ul>
                  </li>
                </ol>
              <div class="text-center">
              @include('partials.imgProgramas.comedores.comedor3',array())
              </div>
          </div>
          </div>
           <div id="tab-4" class="tab-pane row fade">
            <div class="col-md-12 text-left">
               <ol class="text-left listado">
                  <li><p>
                    Interés de la comunidad por tener un comedor:
                    </p>
                    <ul class="text-left listado">
                      <li><p>
                        La comunidad conformada en Comité
                        Comunitario solicita un Comedor a la
                        Coordinación Estatal del Programa.
                      </p></li>
                    </ul>
                  </li>
                  <li>
                    <p>Servicio de alimentación:</p>
                    <ul class="listado">
                      <li><p>
                        Acercarse al Comité Comunitario de su
                        localidad o al Comedor Comunitario y
                        solicitar el servicio del comedor; de ser
                        autorizado por el Comité, y si cumple con
                        los requisitos solicitados, será añadido a
                        una lista de personas beneficiadas.
                      </p></li>
                      <li><p>
                        Cubrir la cuota establecida por el Comité
                        Comunitario, ya sea con recursos
                        económicos (hasta 10 pesos), en especie o
                        trabajo dentro del Comedor, conforme lo
                        autorice el Comité.
                      </p></li>
                    </ul>
                  </li>
                </ol>

              <div class="text-center">
              @include('partials.imgProgramas.comedores.comedor4',array())
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END TABS -->
    </div>

    @endsection
    @section('modals')
    @endsection
    @section('js-extras')
    @endsection