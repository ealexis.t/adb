@extends('layouts.programas')
@include('partials/programas',array())
@section('content')
<div class="container">
    <div class="row head">
        <div class="col-md-12 ">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <img src="{!! asset('/assets/image/programasPng/liconsa.png') !!}" width="150px" height="150px">
            </div>
            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 text-right title_ins" >
            <h1>LICONSA</h1>
                <p>
                Abasto Social de Leche
                </p>
            </div>
        </div>
    </div>
</div>
<div class="row blank">
  <!-- TABS -->
  <div class="col-md-12 tab-style-1">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
      <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
    </ul>
    <div class="tab-content">
      <div id="tab-1" class="tab-pane row fade active in">
        <div class="col-md-12 text-center">
          <p class="margin-bottom-10">
            Apoyar a las personas integrantes de los hogares
            beneficiarios al programa, mediante el acceso al
            consumo de leche fortificada de calidad a bajo
            precio.
          </p>
          <div class="text-center">
          @include('partials.imgProgramas.liconsa.liconsa1',array())
          </div>
        </div>
      </div>
      <div id="tab-2" class="tab-pane row fade">
        <div class="col-md-12 text-center">
        <br>
          <ul class="text-left listado">
            <li><p>
              Niñas y niños de 6 meses a 12 años de edad
            </p></li>
            <li><p>
              Mujeres adolescentes de 13 a 15 años
            </p></li>
            <li><p>
               Mujeres en periodo de gestación o lactancia
            </p></li>
            <li><p>
              Mujeres de 45 a 59 años
            </p></li>
            <li><p>
              Personas con enfermedades crónicas y personas con discapacidad
            </p></li>
          </ul>
          <div class="text-center">
          @include('partials.imgProgramas.liconsa.liconsa2',array())
          </div>
        </div>
      </div>
      <div id="tab-3" class="tab-pane fade">
        <div class="col-md-12 text-center">
           <p class="margin-bottom-10 text-justify">
            Brinda el acceso al consumo de leche fortificada de calidad a muy bajo precio por medio de dos modalidades:
            <br><br>
            <strong>Abasto Comunitario: </strong>
              Entrega de dotación de leche
              fortificada con alto valor nutricional a un precio preferencial
              por litro, transfiriendo un margen de ahorro al hogar
              beneficiario generado por la diferencia entre el precio de la
              leche LICONSA y de la leche comercial.
              <ul class="text-left listado">
                <li><p>
                  Cada beneficiario recibe hasta 4 litros de leche a la
                  semana; el máximo que puede recibir un hogar son
                  24 litros a la semana.
                </p></li>
              </ul>

              <table class="table table-hover table-striped table-bordered">
                <thead>
                  <tr>
                    <th class="text-center">No. de personas</th>
                    <th class="text-center">beneficiarias Dotación semanal</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td  class="text-center">1</td>
                    <td  class="text-center">4</td>
                  </tr>
                  <tr>
                    <td  class="text-center">2</td>
                    <td  class="text-center">8</td>
                  </tr>
                  <tr>
                    <td  class="text-center">3</td>
                    <td  class="text-center">12</td>
                  </tr>
                  <tr>
                    <td  class="text-center">4</td>
                    <td  class="text-center">16</td>
                  </tr>
                  <tr>
                    <td  class="text-center">5</td>
                    <td  class="text-center">20</td>
                  </tr>
                  <tr>
                    <td  class="text-center">6 o más</td>
                    <td  class="text-center">24</td>
                  </tr>
                </tbody>

              </table>
              <p>
                La leche estará disponible para su venta en los días y horarios difundidos en cada punto de venta.
              </p>
              <br><br>
              <h4>Convenios con Actores Sociales</h4>
              <p>Actores sociales que atienden a personas en situación de
vulnerabilidad de acuerdo a las siguientes prioridades:</p>
              <ul class="text-left listado">
                <li><p>
                  Personas que por diferentes situaciones están
                  ingresadas en alguna institución pública o privada
                  de asistencia social.
                </p></li>
                <li><p>
                  Personas que son apoyadas por instituciones
                  privadas de asistencia social que suman esfuerzo
                  con el gobierno, a favor de la nutrición de las niñas
                  y los niños, así como de la población objetivo.
                </p></li>
                <li><p>
                  Atiende a personas por medio de instituciones de
                  gobierno que, por sus objetivos estratégicos, se
                  coordinan entre sí para superar los rezagos
                  socioeconómicos de algunos sectores de la
                  población.
                </p></li>

              </ul>

          </p>
          <div class="text-center">
          @include('partials.imgProgramas.liconsa.liconsa3',array())
          </div>
      </div>
      </div>
       <div id="tab-4" class="tab-pane row fade">
        <div class="col-md-12 text-justify">
              <ul class="text-left listado">
                <li><p>
                  Asegurar que el domicilio del hogar se encuentre
                  dentro de la cobertura del Programa y que en el
                  mismo existan personas con las características
                  antes mencionadas
                </p></li>
                <li><p>
                  Acudir a la lechería el día y en el horario en el cual
                  asiste el personal de Promotoría Social encargado
                  con la siguiente documentación en original o copia
                  para revisión:
                </p></li>
              </ul>
              <br><br>
              <p>a) Presentar cualquiera de las siguientes identificaciones
correspondiente a la persona que pretenda ser Titular:
              </p>
              <ul class="text-left listado">
                <li><p>
                  Credencial para votar con fotografía
                </p></li>
                <li><p>
                  Cartilla del Servicio Militar Nacional
                </p></li>
                <li><p>
                  Pasaporte
                </p></li>
                <li><p>
                  Profesional
                </p></li>
                <li><p>
                  Cédula de Identidad Ciudadana
                </p></li>
                <li><p>
                  Credencial del Instituto Nacional de las Personas Adultas Mayores (INAPAM)
                </p></li>
                <li><p>
                  Constancia de Identidad o de Residencia con
                  fotografía, emitida por autoridad local, expedida en
                  un periodo no mayor de seis meses previo a su
                  presentación
                </p></li>
                <li><p>
                  Formas Migratorias vigentes
                </p></li>
              </ul>
              <br><br>
              <p>b) Contar con cualquiera de los siguientes comprobantes de domicilio
              </p>
              <ul class="text-left listado">
                <li><p>
                  Recibo de luz, agua, predial o teléfono
                </p></li>
                <li><p>
                  Escrito libre de la autoridad local en el que se valide la residencia del solicitante
                </p></li>
                  <p><strong>NOTA:</strong></p><p>                El comprobante deberá ser de fecha reciente (antigüedad máxima de tres meses).</p>
              </ul>
              <br><br>
              <p>
                c) Proporcionar el Acta de Nacimiento de la persona titular y de las personas beneficiarias.
                <br>
                  <br>
                d) Presentar la Clave Única del Registro de Población (CURP) de la persona titular y de las personas beneficiarias.
                <br>
                <br>
                Además de los documentos antes mencionados es necesario que presenten:
              </p>
              <br>
              <ul class="text-left listado">
                <li><p>
                  Constancia médica, o en su caso, copia del carnet
                  perinatal o control de embarazo, u otro documento
                  expedido por instituciones de salud del gobierno
                  federal, estatal o municipal, en la que se haga
                  referencia a su situación de embarazo.
                </p></li>
              </ul>
              <div class="text-center">
              @include('partials.imgProgramas.liconsa.liconsa4',array())
              </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END TABS -->
</div>

@endsection
@section('modals')
@endsection
@section('js-extras')
@endsection