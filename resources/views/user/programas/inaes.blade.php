@extends('layouts.programas')
@include('partials/programas',array())
@section('content')
<div class="container">
    <div class="row head">
        <div class="col-md-12 ">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <img src="{!! asset('/assets/image/programasPng/inaes.png') !!}" width="150px" height="150px">
            </div>
            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 text-right title_ins" >
            <h1>INAES</h1>
                <p>Instituto Nacional de la Economía Social</p>
            </div>
        </div>
    </div>
</div>
<div class="row blank">
  <!-- TABS -->
  <div class="col-md-12 tab-style-1">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
      <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
    </ul>
    <div class="tab-content">
      <div id="tab-1" class="tab-pane row fade active in">
        <div class="col-md-12 text-center">
          <p class="margin-bottom-10">
            Fortalecer capacidades y medios de los organismos
            del sector social y de la economía que adopten
            cualquiera de las formas previstas en el catálogo de
            las Organizaciones del Sector Social de la
            Economía (OSSE), así como personas con
            ingresos debajo de la línea de bienestar integradas
            a grupos sociales que cuenten con iniciativas
            productivas para la inclusión productiva, laboral y
            financiera.
          </p>
          <div class="text-center">
            @include('partials.imgProgramas.inaes.inaes1',array())
          </div>

        </div>
      </div>
      <div id="tab-2" class="tab-pane row fade">
        <div class="col-md-12 text-center">
          <p>
            Organismos del sector social de la economía que
            adopten cualquiera de las formas previstas en el
            catálogo de OSSE, así como personas con
            ingresos por debajo de la línea de bienestar
            integradas con grupos sociales que cuenten con
            iniciativas productivas.
          </p>
          <div class="text-center">
          @include('partials.imgProgramas.inaes.inaes2',array())
          </div>
        </div>
      </div>
      <div id="tab-3" class="tab-pane fade">
        <div class="col-md-12 text-center">
            <h4>Ofrece tres tipos de apoyo:</h4>
          <ul class="text-left listado">
            <li><p>
            <strong>Apoyos a Proyectos Productivos:</strong>
            Apoyos económicos para la
            reinversión y nuevos proyectos.
            </p></li>
            <li><p>
            <strong>Apoyos para el Desarrollo de
            Capacidades:</strong> Apoyos en especie a
            organizaciones que proporcionen
            servicios de formulación de proyectos.
            </p></li>
            <li><p>
            <strong>Apoyos para Banca Social:</strong> Apoyos en
            efectivo o en especie para fomentar y
            apoyar la creación, transformación,
            fortalecimiento, consolidación e
            integración de organizaciones.
            </p></li>
          </ul>
          <div class="text-center">
          @include('partials.imgProgramas.inaes.inaes3',array())
          </div>
      </div>
      </div>
       <div id="tab-4" class="tab-pane row fade">
        <div class="col-md-12 text-center">
            <ul class="text-left listado">
            <li><p>
              Llenar y entregar el formato de solicitud de
              trámite que emite el portal www.gob.mx
            </p></li>
            <li><p>
            Obtener el folio de pre registro en el portal www.gob.mx
            </p></li>
            <li><p>
            Comprometerse a realizar un Proceso de
            Formulación de Proyectos, a través de una
            carta compromiso emitida por el portal
            www.gob.mx
            </p></li>
            <li><p>
            Presentar constancia de Formulación de
            Proyectos, la cual es emitida al finalizar el
            Proceso de Formulación de Proyectos en el portal
            www.gob.mx
            </p></li>
            <li><p>
            Acreditar residencia:
            Presentar copia de comprobante de
            domicilio de cada uno de los hogares
            de los solicitantes
            </p></li>
            <li><p>
            Manifestar su interés de recibir apoyos del
            Programa
            </p></li>
            <li><p>
            Acreditar identidad:
            o Presentar copia de identificación
            oficial de cada una de las personas
            solicitantes
            </p></li>
            <li><p>
            Contar con un proyecto productivo
            </p></li>
            </ul>
            <div class="text-center">
            @include('partials.imgProgramas.inaes.inaes4',array())
            </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END TABS -->
</div>

@endsection
@section('modals')
@endsection
@section('js-extras')
@endsection