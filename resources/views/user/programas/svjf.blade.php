@extends('layouts.programas')
    @include('partials/programas',array())
@section('content')

<div class="container">
    <div class="row head">
        <div class="col-md-12 ">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              @include('partials.iconosProgramas.svjf',array())
            </div>
            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 text-right title_ins" >
            <h1>SVJF</h1>
                <p>Seguro de Vida para Jefas de Familia</p>
            </div>
        </div>
    </div>
</div>
<div class="row blank">
  <!-- TABS -->
  <div class="col-md-12 tab-style-1">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
      <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
        <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
    </ul>
    <div class="tab-content">
      <div id="tab-1" class="tab-pane row fade active in">
        <div class="col-md-12 text-left">
          <p class="margin-bottom-10">Contribuir a dotar de esquemas de seguridad social que

protejan el bienestar socioeconómico de la población en

situación de carencia o pobreza, mediante la incorporación de

jefas de familia en condición de pobreza, vulnerabilidad por

carencias sociales o vulnerabilidad por ingresos a un seguro

de vida.</p>

        </div>
        <div class="text-center">
          @include('partials.imgProgramas.svjf.svjf1',array())
        </div>
      </div>
      <div id="tab-2" class="tab-pane row fade">
        <div class="col-md-12 text-left">
          <ul class="text-left listado">
                <li><p>Jefas de familia que se encuentran en situación de

pobreza, en situación de vulnerabilidad por carencias

sociales o en situación de vulnerabilidad por ingresos.</p></li>
            </ul>
            <h4>Población beneficiada del Seguro de Vida para Jefas de

Familia</h4>
                        <ul class="text-left listado">
                <li><p>Niñas, niños, adolescentes y jóvenes hasta 23 años de

edad, en condición de orfandad materna y se

encuentren cursando estudios en el sistema educativo

nacional, así como niñas y niños desde recién nacidos

hasta 5 años de edad, cuya jefa de familia se

encontraba bajo el esquema de aseguramiento del

programa.</p></li>
            </ul>



        </div>
        <div class="text-center">
          @include('partials.imgProgramas.svjf.svjf2',array())
        </div>
      </div>
      <div id="tab-3" class="tab-pane fade">
        <div class="col-md-12 text-left">
            <ul class="text-left listado">
                <li><p>Otorga un apoyo monetario directo mensual, que se

entregará a las personas beneficiarias de manera

bimestral.</p></li>
                <li><p>Se entrega a aquellos que hayan quedado en estado

de orfandad a partir de la entrada en vigor del presente

programa (1 marzo de 2013) y se otorgarán con

retroactividad al día posterior de la fecha de

fallecimiento de la jefa de familia.</p></li>
            </ul>

            <table class="table table-hover table-striped table-bordered">
                <thead>
                  <tr>
                    <th class="text-center">Nivel educativo</th>
                    <th class="text-center">Monto mensual (pesos)</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td  class="text-center">De recién nacido

y hasta preescolar</td>
                    <td  class="text-center">330</td>
                  </tr>
                  <tr>
                    <td  class="text-center">Primaria</td>
                    <td  class="text-center">550</td>
                  </tr>
                  <tr>
                    <td  class="text-center">Secundaria</td>
                    <td  class="text-center">770</td>
                  </tr>
                  <tr>
                    <td  class="text-center">Medio Superior</td>
                    <td  class="text-center">990</td>
                  </tr>
                  <tr>
                    <td  class="text-center">Superior</td>
                    <td  class="text-center">1,100 y hasta 2,040 pesos, en

los casos de excepción que

determine el Comité Técnico</td>
                  </tr>

                </tbody>

              </table>

      </div>
        <div class="text-center">
          @include('partials.imgProgramas.svjf.svjf3',array())
        </div>
      </div>
       <div id="tab-4" class="tab-pane row fade">
        <div class="col-md-12 text-left">
            <h4>Apoyos para Impulso Productivo</h4>
            <br>
              <h4>1. Pre-registro</h4>
                <ul class="text-left listado">
                    <li><p>Ser jefa de Familia</p></li>
                    <li><p>Tener hijas e hijos (recién nacidos hasta

23 años).</p></li>
                    <li><p>Padecer carencia de acceso a la

alimentación, seguridad social, servicios

de salud, calidad y espacios en la

vivienda, rezago educativo o tener

ingresos bajos.</p></li>
                    <li><p>Presentar original para cotejo de alguno

de los siguientes documentos:</p>
                    <ul class="text-left listado">
                        <li><p>Credencial para Votar vigente</p></li>
                        <li><p>Cédula Profesional</p></li>
                        <li><p>Pasaporte vigente</p></li>
                        <li><p>Forma Migratoria</p></li>
                        <li><p>Cédula de Identidad Ciudadana o

Cédula de Identidad Personal.</p></li>
                        </ul>

                    </li>
                </ul>
              <h4>2. Registro de Beneficiarios</h4>

                <ul class="text-left listado">
                    <li><p>Personas Responsables de hijas e hijos en estado
de orfandad materna menores de 18 años de edad.</p></li>
                    <li><p>Haber sido designado y/o designada por

la Jefa de Familia como persona

responsable de las y los hijos o tener la

custodia legal</p></li>
                    <li><p>
                        Presentar solicitud original de pre registro

y registro
                        </p></li>
                    <li>
                    <p>
                        Presentar original para cotejo de alguno

de los siguientes documentos:
                        </p>
                        <ul class="text-left listado">
                        <li><p>
Credencial para Votar vigente
                        </p></li>
                                                    <li><p>
Cédula Profesional
                        </p></li>
                                                                                <li><p>
Pasaporte vigente
                        </p></li>
                     <li><p>
Forma Migratoria
                        </p></li>
                                                 <li><p>
Cédula de Identidad Ciudadana o

Cédula de Identidad Personal
                        </p></li>
                        </ul>
                    </li>
            <li><p>Acta o certificado de defunción de la

madre</p></li>
                <li><p>Acta de nacimiento de los hijos e hijas</p></li>
                <li><p>CURP de los hijos e hijas y persona

responsable</p></li>
                    <li><p>Comprobante de domicilio como: recibo

de luz, agua, teléfono, impuesto predial</p></li>
                      <li><p>Contestar el Cuestionario Único de
Información Socioeconómica (CUIS)</p></li>
                      <li><p>Comprobante de estudios.</p></li>

                </ul></li>
            </ul>
        </div>
        <div class="text-center">
          @include('partials.imgProgramas.svjf.svjf4',array())
        </div>
      </div>
    </div>
  </div>
  <!-- END TABS -->
</div>

@endsection
@section('modals')
@endsection
@section('js-extras')
@endsection