@extends('layouts.programas')
@include('partials/programas',array())
@section('content')

<div class="container">
    <div class="row head">
        <div class="col-md-12 ">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                 <img src="/assets/image/ico_conadis-06.png" width="150px" height="150px">
            </div>
            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 text-right title_ins" >
            <h1>CONADIS</h1>
                <p>Consejo Nacional para el Desarrollo y la Inclusión de las Personas con Discapacidad</p>
            </div>
        </div>
    </div>
</div>
<div class="row blank">
  <!-- TABS -->
  <div class="col-md-12 tab-style-1">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
      <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
        <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
    </ul>
    <div class="tab-content">
      <div id="tab-1" class="tab-pane row fade active in">
        <div class="col-md-12 text-center">
          <p class="margin-bottom-10">Asegurar el ejercicio de los derechos de las personas con discapacidad y contribuir a su desarrollo integral.</p>
            <img src="/assets/image/ico_conadis-07.png">
        </div>
      </div>
      <div id="tab-2" class="tab-pane row fade">
        <div class="col-md-12 text-center">
          <p>Población que presenta algún tipo de discapacidad.</p>
             <img src="/assets/image/ico_conadis-08.png">
        </div>
      </div>
      <div id="tab-3" class="tab-pane fade">
        <div class="col-md-12 text-center">
            <ul class="text-left listado">
          <li><p>Coordina y elabora el Programa Nacional para el Desarrollo y la Inclusión de las Personas con Discapacidad.</p></li>
          <li><p>Promueve los derechos de las personas con discapacidad.</p></li>
          <li><p>Promueve los espacios para el fácil acceso de las personas con discapacidad.</p></li>
          <li><p>Promueve la elaboración, publicación y difusión de materiales sobre el desarrollo e inclusión social de las personas con discapacidad.</p></li>
          <li><p>Impulsa la cultura de dignidad y respeto hacia las personas con discapacidad.</p></li>
          </ul>
           <img src="/assets/image/ico_conadis-07.png">
      </div>
      </div>
       <div id="tab-4" class="tab-pane row fade">
        <div class="col-md-12 text-center">
            <ul class="text-left listado">
            <li><p>Presentar alguna discapacidad, ya sea motora, sensorial, intelectual o mental.</p></li>
            <li><p>Presentar algún documento que acredite la discapacidad.</p></li>
            <li><p>Acudir a las oficinas del CONADIS o comunicarse a los teléfonos que aparecen en la página de internet.</p></li>

            </ul>
            <img src="/assets/image/ico_conadis-07.png">
        </div>
      </div>
    </div>
  </div>
  <!-- END TABS -->
</div>

@endsection
@section('modals')
@endsection
@section('js-extras')
@endsection