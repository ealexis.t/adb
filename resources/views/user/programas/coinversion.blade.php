    @extends('layouts.programas')
    @include('partials/programas',array())
    @section('content')

    <div class="container">
        <div class="row head">
            <div class="col-md-12 ">
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <img src="/assets/image/programasPng/coinversion.png" width="150px" height="150px">
                </div>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 text-right title_ins" >
                <h1>INDESOL</h1>
                    <p>Coinversión Social</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row blank">
      <!-- TABS -->
      <div class="col-md-12 tab-style-1">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
          <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
          <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
            <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
        </ul>
        <div class="tab-content">
          <div id="tab-1" class="tab-pane row fade active in">
            <div class="col-md-12 text-left">
              <p class="margin-bottom-10">
                Fortalecer y vincular a los actores sociales para
                que, a través del fomento y apoyo a sus actividades,
                promuevan la cohesión y el capital social de
                grupos, comunidades o regiones que viven en
                situación de vulnerabilidad o exclusión.
              </p>
              <div class="text-center">
                @include('partials.imgProgramas.coinversion.coinversion1',array())
              </div>

            </div>
          </div>
          <div id="tab-2" class="tab-pane row fade">
            <div class="col-md-12 text-left">
              Organizaciones de la Sociedad Civil, Instituciones
              de Educación Superior y Centros de Investigación
              que cuenten con proyectos de desarrollo social
              que apoyen al desarrollo de grupos y regiones que
              viven en situación de pobreza o exclusión.
                <div class="text-center">
                  @include('partials.imgProgramas.coinversion.coinversion2',array())
                </div>
            </div>
          </div>
          <div id="tab-3" class="tab-pane fade">
            <div class="col-md-12 text-left">
              <ul class="listado">
                <li><p>
                  Otorga recursos públicos concursables para la
                  ejecución de proyectos, a través de
                  convocatorias públicas.
                </p></li>
                <li><p>
                  Proyectos apoyados reciben recursos públicos catalogados como subsidios.
                </p></li>
                <li><p>
                  Los proyectos deberán alinearse a alguna de las siguientes vertientes:
                </p>
                  <ul>
                    <li><p>
                      Promoción del Desarrollo Humano y Social
                    </p></li>
                    <li><p>
                      Fortalecimiento, Capacitación y Sistematización
                    </p></li>
                    <li><p>
                      Investigación
                    </p></li>
                  </ul>
                </li>
                <li><p>
                  El monto máximo de recursos que se podrá
                  otorgar por proyecto se establecerá en la
                  convocatoria, en ningún caso se podrá otorgar
                  a un proyecto más de 1 millón de pesos.
                </p></li>

              </ul>
              <div class="text-center">
              @include('partials.imgProgramas.coinversion.coinversion3',array())
              </div>
          </div>
          </div>
           <div id="tab-4" class="tab-pane row fade">
            <div class="col-md-12 text-left">
              <ul>
                <li><p>
                  Presentar el Formato de Solicitud de Apoyo
                  Económico mediante el Sistema Electrónico
                  que el Programa disponga, en la dirección
                  electrónica www.indesol.gob.mx.
                </p></li>
                <li><p>
                  Adjuntar al proyecto, la Constancia de
                  Situación Fiscal emitida por el Servicio de
                  Administración Tributaria.
                </p></li>
                <li><p>
                  Registrar en el Formato de Solicitud de
                  Apoyo Económico para la Ejecución de
                  Proyectos, la aportación del Actor Social.
                </p></li>
                <li><p>
                  Los Actores Sociales tienen derecho a
                  presentar un solo proyecto en el ejercicio
                  fiscal correspondiente, en caso de que el
                  Indesol emita una convocatoria en
                  coinversión con otra instancia, podrá
                  presentar un segundo proyecto.
                </p></li>
              </ul>
              <div class="text-center">
              @include('partials.imgProgramas.coinversion.coinversion4',array())
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END TABS -->
    </div>

    @endsection
    @section('modals')
    @endsection
    @section('js-extras')
    @endsection