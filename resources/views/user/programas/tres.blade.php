    @extends('layouts.programas')
    @include('partials/programas',array())
    @section('content')

    <div class="container">
        <div class="row head">
            <div class="col-md-12 ">
              <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                <img src="/assets/image/programasPng/3x1.png" width="150px" height="150px">
              </div>
              <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 text-right title_ins" >
                <h1>3x1</h1>
                    <p>3x1 para migrantes</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row blank">
      <!-- TABS -->
      <div class="col-md-12 tab-style-1">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
          <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
          <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
            <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
        </ul>
        <div class="tab-content">
          <div id="tab-1" class="tab-pane row fade active in">
            <div class="col-md-12 text-left">
              <p class="margin-bottom-10">Contribuir a fortalecer la participación social para impulsar el desarrollo comunitario mediante la inversión en Proyectos de Infraestructura Social, Servicios Comunitarios, Educativos y/o Proyectos Productivos cofinanciados por los tres órdenes de gobierno y organizaciones de mexicanas y mexicanos en el extranjero</p>
              <div class="text-center">
              @include('partials.imgProgramas.3x1.tres1',array())
              </div>
            </div>
          </div>
          <div id="tab-2" class="tab-pane row fade">
            <div class="col-md-12 text-center">
              <p>Localidades en México que las y los migrantes decidan apoyar así como a sus familias.</p>
               

                         
              <div class="text-center">
              @include('partials.imgProgramas.3x1.tres2',array())
              </div>
            </div>
          </div>
          <div id="tab-3" class="tab-pane fade">
            <div class="col-md-12 text-left">
                <h3>Otorga apoyos económicos para:</h3>
                <ul class="text-left listado">
              <li><p>Proyectos de Infraestructura Social:</p><ul class="text-left listado"><li><p>Realiza proyectos de infraestructura social básica y para el mejoramiento urbano y/o de protección del medio ambiente.</p></li>
             <li><p>Ofrece un monto máximo de apoyo federal de 1’000,000 pesos.</p></li></ul>
                    
             </li>
                </ul>
              
                <p>Se requiere contar con la siguiente mezcla

financiera 25% corresponderá al Gobierno Federal;

el 25% a los Clubes u Organizaciones de

Migrantes y el 50% a gobiernos de las entidades

federativas y municipios.</p>

                <ul class="text-left listado">
                              <li><p>Proyectos de servicios comunitarios:</p>
                                 <ul class="text-left listado">
                                      <li><p>Becas académicas</p>
                                        <p>De acuerdo al nivel escolar, se tendrá el siguiente monto máximo de apoyo de beca por persona y ciclo escolar: </p>
                                     <ul class="text-left listado">
                                     <li><p>Preescolar 1,750 pesos</p></li>
                                     <li><p>Primaria 1,750 pesos </p></li>
                                     <li><p>Secundaria 5,400 pesos</p></li>
                                     <li><p>Bachillerato 9,250 pesos</p></li>
                                     <li><p>Superior 10,000 pesos</p></li>
                                     </ul>
                                          </li>
                                     <li><p>Espacios de beneficio comunitario</p><p>Ofrece un monto máximo de apoyo federal de

1’000,000 pesos.</p></li>
                                 </ul>
                                  <p>Se requiere contar con la participación financiera

de los Clubes de Migrantes y se acepta la

participación financiera de los gobiernos estatales

y municipales.</p>
                            </li>
                    <li><p>Proyectos Educativos:</p>
                        <ul class="text-left listado">
                                     <li><p>Equipamiento de escuelas, con un

monto máximo de apoyo federal de

250,000 pesos.</p></li>
                                     <li><p>Mejoramiento de infraestructura escolar, con un monto máximo de apoyo federal de 400,000 pesos.</p>
                                        <p>Se requiere contar con la participación financiera de los Clubes de Migrantes y se acepta la participación financiera de los gobiernos estatales  y municipales.</p>
                                        </li>
                                     </ul>
                                          </li>
                    
                                        <li><p>Proyectos Productivos: </p>
                        <ul class="text-left listado">
                                     <li><p>Comunitarios: benefician al menos a cinco familias, el monto máximo de apoyo federal es de 500,000 pesos</p>
                                        <p>Se requiere contar con la siguiente mezcla financiera: 25% corresponderá al Gobierno Federal; el 25% a los Clubes u Organizaciones de Migrantes y el 50% a gobiernos de las entidades federativas y municipios.</p>
                            
                                    </li>
                                      <li><p>Individuales: benefician a una familia, el monto máximo de apoyo federal es de 250,000  pesos y el financiamiento es 1 x 1: Migrante 50% y SEDESOL 50%. </p>
                            
                                    </li>
                             <li><p>Servicios de capacitación empresarial para proyectos productivos: son servicios de acompañamiento y asesoría técnica que tienen el propósito de aumentar las posibilidades de éxito comercial de los proyectos productivos apoyados por el programa.</p>
                                       
                                    </li>
                                     </ul>
                                          </li>

                
                    
                    

                    
                </ul>
                
              <div class="text-center">
              @include('partials.imgProgramas.3x1.tres3',array())
              </div>
          </div>
          </div>
           <div id="tab-4" class="tab-pane row fade">
            <div class="col-md-12 text-left">
                <p>Para participar en el Programa se deberá cumplir con lo siguiente en cada proyecto:</p>
                <ul class="text-left listado">
                <li><p>Proyectos de infraestructura social:</p>
                    
                    <ul class="text-left listado">
                        <li><p>Presentar copia del Formato 3x1-C “Toma de Nota” del Club u Organización de Migrantes vigente.</p></li>
                        <li><p>Presentar original y copia del Formato 3x1-B ”Solicitud de Apoyo para Proyecto de Infraestructura Social, Servicios Comunitarios o Educativos”</p></li>
                        <li><p>Indicar en la solicitud del proyecto los datos del representante en México (Copia de identificación oficial y comprobante de domicilio)</p></li>
                        <li><p>Presentar copia de un estado de cuenta bancario actualizado del Club u Organización de Migrantes, y/o del Representante del Club, en el cual se demuestre los recursos que aportarán para los proyectos</p></li>
                        <li><p>Presentar el expediente técnico del proyecto.</p></li>
                        </ul>
  </li>
                    <li><p>Proyectos de infraestructura social:</p>
                        <ul class="text-left listado">
                        <li><p>Presentar copia del Formato 3x1-C “Toma de Nota” del Club u Organización de Migrantes vigente</p></li>
                        <li><p>Presentar original y copia del Formato 3x1-F "Solicitud de Apoyo para Proyecto Productivo" </p></li>
                        <li><p>Presentar el formato 3x1-H en el cual se mencionan las características de las familias beneficiarias de un Proyecto Productivo, Familiar o Comunitario” </p></li>
                        <li><p>Designar a un representante en México e  indicar su nombre en el Formato3x1-F junto a una copia de su identificación oficial y comprobante de domicilio</p></li>
                        <li><p>Presentar en original y copia el Formato 3x1-G “Plan de Negocios”  </p></li>
                        <li><p>Credencial del INAPAM</p></li>
                        <li><p>Constancia de identidad de residencia con fotografía</p></li>
                        <li><p>Presentar copia de un estado de cuenta bancario actualizado del Club u Organización de Migrantes, y/o del Representante del Club, en el cual se demuestre los recursos que aportarán para los proyectos</p></li>
                        </ul>
                    </li>

                    <li><p>Proyectos productivos individuales:</p>
                        <ul class="text-left listado">
                        <li><p>Presentar copia del Formato 3x1-C “Toma Nota” del Club u Organización de Migrantes, vigente.</p></li>
                        <li><p>Presentar copia de una identificación que acredite la nacionalidad mexicana.</p></li>
                        <li><p>Presentar copia de comprobante de domicilio.</p></li>
                        <li><p>Presentar original y copia del Formato 3x1-F "Solicitud de Apoyo para Proyecto Productivo". </p></li>
                        <li><p>Designar a un representante en México e  indicar su nombre en el Formato3x1-F junto a una copia de su identificación oficial y comprobante de domicilio.</p></li>
                        <li><p>Presentar en original y copia el Formato 3x1-G “Plan de Negocios”.</p></li>
                        <li><p>Presentar copia de un estado de cuenta bancario actualizado del Club u Organización de Migrantes, y/o del Club en México, en el que se evidencien los recursos que aportarán. </p></li>
                        </ul>
                    </li>
                    
                    <li><p>Servicios de capacitación empresarial para proyectos productivos:</p>
                        <ul class="text-left listado">
                        <li><p>Escrito libre donde se requieran los servicios de capacitación empresarial </p></li>
                        </ul>
                    </li>

                                                                                
                </ul>
                                    
                <div class="text-center">
                @include('partials.imgProgramas.3x1.tres4',array())
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END TABS -->
    </div>

    @endsection
    @section('modals')
    @endsection
    @section('js-extras')
    @endsection