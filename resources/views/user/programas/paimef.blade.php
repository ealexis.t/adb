@extends('layouts.programas')
@include('partials/programas',array())
@section('content')
<div class="container">
    <div class="row head">
        <div class="col-md-12 ">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <img src="/assets/image/programasPng/paimef.png" width="150px" height="150px">
            </div>
            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 text-right title_ins" >
            <h1>PAIMEF</h1>
                <p>
                Programa de Apoyo a Instacias de Mujeres en las Entidades Federativas
                </p>
            </div>
        </div>
    </div>
</div>
<div class="row blank">
  <!-- TABS -->
  <div class="col-md-12 tab-style-1">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
      <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
    </ul>
    <div class="tab-content">
      <div id="tab-1" class="tab-pane row fade active in">
        <div class="col-md-12 text-center">
          <p class="margin-bottom-10">
           Prevenir y atender la violencia contra las mujeres por medio de las acciones que promueven y operan las Instancias de Mujeres en las Entidades Federativas (IMEF) en coordinación con  Organizaciones de la Sociedad Civil (OSC), gobiernos locales (estatales y municipales) e instituciones académicas.
          </p>
          <div class="text-center">
          @include('partials.imgProgramas.paimef.paimef1',array())
          </div>
        </div>
      </div>
      <div id="tab-2" class="tab-pane row fade">
        <div class="col-md-12 text-center">
        <br>
          <p class="margin-bottom-10">
              Mujeres en situación de violencia  por motivos de género.
            </p>
          <div class="text-center">
          @include('partials.imgProgramas.paimef.paimef2',array())
          </div>
        </div>
      </div>
      <div id="tab-3" class="tab-pane fade">
        <div class="col-md-12 text-left">
           <p class="margin-bottom-10 text-justify">
            El PAIMEF otorga recursos para que las IMEF realicen las siguientes acciones: 
            </p>
            <p>Vertiente A: Institucionalización de la prevención y la atención de la violencia contra las mujeres.</p>
              <ul class="text-left listado">
                <li><p>
                  Realiza capacitaciones para el personal del servicio público y otros actores sociales, en temas de prevención y atención de la violencia.
                </p></li>
                <li><p>
                  Desarrolla programas de trabajo interinstitucionales sobre prevención y atención de la violencia contra las mujeres.
                </p></li>
              </ul>
            <p>Vertiente B: Prevención de la violencia contra las mujeres.</p>
            
             <ul class="text-left listado">
                <li><p>
                  Realiza campañas de difusión masiva sobre la violencia contra las mujeres por medio de jornadas, ferias de servicios, medios masivos de comunicación (televisión, radio e internet), teatro, cine-debate, entre otras actividades.
                </p></li>
                <li><p>
                  Realiza procesos de capacitación y sensibilización sobre la violencia contra las mujeres dirigidos a grupos específicos de la población.
                </p></li>
                 <li>
                     <p>
                         Acompaña y apoya al trabajo de las personas promotoras locales de una vida libre de violencia.
                     </p>
                 </li>
              </ul>
            <p>Vertiente C: Atención especializada a las mujeres en situación de violencia y, en su caso, a sus hijas e hijos y personas allegadas.</p>
                         <ul class="text-left listado">
                <li><p>
                  Apoyo en la creación y el fortalecimiento de servicios especializados para mujeres en situación de violencia: Servicios de Prevención y Atención Externa (SPAE) y Servicios de Alojamiento, Protección y Atención (SAPA).
                </p></li>
                <li><p>
                  Apoyo y acompañamiento a mujeres víctimas de la violencia de género, así como desarrollo de  habilidades y destrezas laborales o productivas de las mujeres atendidas en las Unidades Apoyadas por el PAIMEF.
                </p></li>
              </ul>
              
          <div class="text-center">
          @include('partials.imgProgramas.paimef.paimef3',array())
          </div>
      </div>
      </div>
       <div id="tab-4" class="tab-pane row fade">
        <div class="col-md-12 text-center">
              <p class="margin-bottom10">Acudir a la instancia de mujeres en tu estado (instituto, secretaría o consejo de la mujer).</p>
              <div class="text-center">
              @include('partials.imgProgramas.paimef.paimef4',array())
              </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END TABS -->
</div>

@endsection
@section('modals')
@endsection
@section('js-extras')
@endsection