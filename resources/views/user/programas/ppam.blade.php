@extends('layouts.programas')
    @include('partials/programas',array())
@section('content')

<div class="container">
    <div class="row head">
        <div class="col-md-12 ">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              @include('partials.iconosProgramas.ppam',array())
            </div>
            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 text-right title_ins" >
            <h1>PPAM</h1>
                <p>Programa Pensión para Adultos Mayores</p>
            </div>
        </div>
    </div>
</div>
<div class="row blank">
  <!-- TABS -->
  <div class="col-md-12 tab-style-1">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
      <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
        <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
    </ul>
    <div class="tab-content">
      <div id="tab-1" class="tab-pane row fade active in">
        <div class="col-md-12 text-center">
          <p class="margin-bottom-10">Asegurar un ingreso mínimo y apoyos de protección

social a los Adultos Mayores de 65 años de edad en

adelante.</p>

        </div>
        <div class="text-center">
            @include('partials.imgProgramas.ppam.ppam1',array())
        </div>
      </div>
      <div id="tab-2" class="tab-pane row fade">
        <div class="col-md-12 text-center">
          <p>Adultos mayores de 65 o más años, mexicanos por

nacimiento o con un mínimo de 25 años de residencia en

el país que no reciben ingresos por jubilación o pensión

de tipo contributivo, superior a 1,092 pesos.</p>
        </div>
        <div class="text-center">
           @include('partials.imgProgramas.ppam.ppam2',array())
        </div>
      </div>
      <div id="tab-3" class="tab-pane fade">
        <div class="col-md-12 text-left">
            <h4>Ofrece dos tipos de apoyos:</h4>
            <ul class="text-left listado">
          <li><p>Apoyos Económicos:</p>
                    <ul class="text-left listado">
                        <li><p>Apoyo económico mensual con entregas bimensuales de 580 pesos.</p></li>
                        <li><p>Apoyo económico de pago de marcha por

única ocasión de 1,160 pesos.</p></li>
                    </ul>
                </li>
          <li><p>Acciones de Protección Social y Participación Comunitaria</p>
            <ul class="text-left listado">
                <li><p>Apoyos para el acceso a servicios y

productos financieros de hasta 300 pesos

por beneficiario por año, incluyendo el

otorgamiento de una tarjeta electrónica

asociada a una cuenta bancaria en la que

reciban su apoyo económico.</p></li>
                <li><p>Apoyos de hasta 85 pesos por beneficiario

por año, para que el mantenimiento y

administración de las cuentas bancarias de

las personas beneficiarias no afecten el

monto de sus apoyos económicos

mensuales, incluyendo la reposición de las

tarjetas necesarias para recibir los apoyos.</p></li>
                <li><p>Acciones para aminorar el deterioro de la

salud física y mental a través de la Red

Social, mediante actividades relacionadas

con:</p>
                <ul class="text-left listado">
                    <li>
                            <p>Promoción de los Derechos Humanos</p>
                    </li>
                                        <li>
                            <p>Desarrollo Personal</p>
                    </li>
                                        <li>
                            <p>Cuidados de la Salud</p>
                    </li>
                                        <li>
                            <p>Cuidado del Medio Ambiente</p>
                    </li>
                     <li>
                        <p>Constitución y desarrollo de Comités Comunitarios.</p>
                    </li>
                    <li>
                        <p>Otros temas en beneficio de la población de Adultos Mayores.</p>
                    </li>
                </ul>
                </li>

                <li>
                  <p>
                      Apoyos que atenúan los riesgos por

  pérdidas en el ingreso o salud, que

  promueven:
                      </p>
                    <ul class="text-left listado">
                        <li>
                        <p>La obtención de la Credencial del INAPAM.
                        </p>
                        </li>
                                                <li>
                        <p>El acceso a los servicios de salud

(Seguro Popular).

Ficha: PAM

capacitación de los cuidadores de la

población.
                        </p>
                        </li>
                    </ul>
                </li>

              </ul>
            </li>
          </ul>
      </div>
          <div class="text-center">
              @include('partials.imgProgramas.ppam.ppam3',array())
          </div>
      </div>
       <div id="tab-4" class="tab-pane row fade">
        <div class="col-md-12 text-left">
            <h4>Apoyos para Impulso Productivo</h4>
            <br>
            <h4>Pre-registro</h4>

                <ul class="text-left listado">
                    <li><p>Tener 65 años en adelante.</p></li>
                    <li><p>Acudir a las Sedes de Atención y realizar su solicitud de incorporación al Programa.</p></li>
                </ul>
                <p>Para acreditar su identidad</p>
<ul class="text-left listado">
                    <li><p>Entregar copia y presentar original para cotejo de

alguno de los siguientes documentos:</p>
                    <ul class="text-left listado">
                            <li><p>Credencial para votar vigente</p></li>
                            <li><p>Pasaporte vigente</p></li>
                            <li><p>Cédula Profesional</p></li>
                            <li><p>Credencial del INAPAM</p></li>
                            <li><p>Cartilla del Servicio Militar Nacional.</p></li>
                            <li><p>Formas Migratorias / Cédula de Identidad Ciudadana.</p></li>
                    </ul>
                    </li>

                </ul>
                <p>Para acreditar su edad</p>
                <ul class="text-left listado">
                            <li><p>Entregar copia del Acta de Nacimiento. En caso de

no contar con ella, podrá hacerlo con alguno de los

documentos arriba mencionados con excepción de

la Cédula Profesional.</p></li>
                            <li><p>Entregar copia de Clave Única de Registro de

Población (CURP)</p></li>
                    </ul>
                <p>Para los solicitantes no nacidos en México</p>
               <ul class="text-left listado">
                            <li><p>Presentar un documento oficial de las autoridades

migratorias mexicanas que acredite su identidad y

edad, así como su permanencia en el país por más

de 25 años.</p></li>
                    </ul>
                <p>Para acreditar su residencia</p>
                <ul class="text-left listado">
                            <li><p>Entregar copia y presentar original para cotejo de

alguno de los siguientes documentos con

antigüedad no mayor a tres meses: energía

eléctrica, agua, teléfono, impuesto predial.</p></li>
                    </ul>
                <p><strong>NOTA</strong></p>
                <p>La entrega de los apoyos económicos directos, se hará

en efectivo o transferencia electrónica, en función de la

infraestructura bancaria disponible.</p>

        </div>
        <div class="text-center">
            @include('partials.imgProgramas.ppam.ppam4',array())
        </div>
      </div>
    </div>
  </div>
  <!-- END TABS -->
</div>

@endsection
@section('modals')
@endsection
@section('js-extras')
@endsection