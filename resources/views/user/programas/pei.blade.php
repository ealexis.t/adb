    @extends('layouts.programas')
    @include('partials/programas',array())
    @section('content')

    <div class="container">
        <div class="row head">
            <div class="col-md-12 ">
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                  <img src="/assets/image/programasPng/pei.png" width="150px" height="150px">
                </div>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 text-right title_ins" >
                <h1>PEI</h1>
                    <p>Programa de Estancias Infantiles</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row blank">
      <!-- TABS -->
      <div class="col-md-12 tab-style-1">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
          <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
          <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
            <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
        </ul>
        <div class="tab-content">
          <div id="tab-1" class="tab-pane row fade active in">
            <div class="col-md-12 text-left">
              <p class="margin-bottom-10">Para madres, padres solos o tutores que trabajen busquen empleo o estudien:</p>
                <ul class="text-left listado">
              <li><p>Mejorar las condiciones de acceso y permanencia en el mercado laboral de las Madres, Padres solos y Tutores que trabajan, buscan empleo o estudian, mediante el acceso a los servicios de cuidado y atención infantil, como un esquema de seguridad social.</p></li>
              </ul>
                 <p class="margin-bottom-10">Personas que deseen establecer y operar una Estancia Infantil:</p>

                            <ul class="text-left listado">
              <li><p>Facilitar a través de subsidios la apertura y operación de una estancia infantil</p></li>
              </ul>
              <div class="text-center">
              @include('partials.imgProgramas.pei.pei1',array())
              </div>
            </div>
          </div>
          <div id="tab-2" class="tab-pane row fade">
            <div class="col-md-12 text-left">
              <p>Para madres, padres solos o tutores que trabajen busquen empleo o estudien.</p>
                <ul class="text-left listado">
              <li><p>Madres, padres solos o tutores que trabajen, busquen empleo o estudien, que cuidan al menos una niña o niño de entre 1 y hasta un día antes de cumplir 4 años de edad, o en el caso de niños con discapacidad de entre 1 y hasta un día antes de cumplir 6 años de edad.</p></li>
              </ul>
                 <p class="margin-bottom-10">Personas que deseen establecer y operar una Estancia Infantil.</p>

                            <ul class="text-left listado">
              <li><p>A las personas físicas o morales que deseen establecer y operar una Estancia Infantil afiliada al Programa, o que cuenten con espacios en los que se brinde o pretenda brindar el servicio de cuidado y atención infantil, a las niñas y niños de las Madres Trabajadoras, Padres Solos y Tutores.</p></li>
              </ul>
              <div class="text-center">
              @include('partials.imgProgramas.pei.pei2',array())
              </div>
            </div>
          </div>
          <div id="tab-3" class="tab-pane fade">
            <div class="col-md-12 text-left">
                <p>Para madres, padres solos o tutores que trabajen busquen empleo o estudien.</p>
                <ul class="text-left listado">
              <li><p>El Gobierno Federal, por conducto de la SEDESOL, cubre el costo de los servicios de cuidado y atención infantil de la siguiente forma:</p><ul class="text-left listado"><li><p>Hasta por 900 pesos mensuales por cada niña o niño de entre 1 a 3 años 11 meses de edad y hasta un día antes de cumplir los 4 años.</p></li><li><p>Hasta por 1,800 pesos mensuales por

cada niña o niño con alguna discapacidad

de entre 1 a 5 años 11 meses de edad y

hasta un día antes de cumplir los 6 años.</p></li></ul></li>
              <li><p>Brinda un máximo de tres apoyos por hogar,

excepto en caso de nacimientos múltiples.</p></li>
              </ul>
                <p>Este apoyo se entrega a la persona Responsable de la

estancia infantil, y la madre, padre o tutor o tutora, debe

cubrir la diferencia por el servicio de cuidado infantil en

la Estancia Infantil.</p>
                <p><strong>NOTA</strong></p>
                <p>Personas que deseen establecer y operar una Estancia

Infantil</p><ul class="text-left listado">
                              <li><p>Se brinda apoyo inicial y único de hasta 70,000

pesos para adecuar el lugar, comprar materiales

para la atención de las niñas y los niños, y cumplir

con las medidas de seguridad solicitadas para la

estancia infantil.</p></li>
              </ul>
              <div class="text-center">
              @include('partials.imgProgramas.pei.pei3',array())
              </div>
          </div>
          </div>
           <div id="tab-4" class="tab-pane row fade">
            <div class="col-md-12 text-left">
                <p>Para madres, padres solos o tutores que trabajen busquen empleo o estudien.</p>
                <ul class="text-left listado">
                <li><p>Entregar el formato de Solicitud de Apoyo a

Madres Trabajadoras y Padres solos, en las

Oficinas de la Delegación o en la Estancia Infantil

afiliada al programa.

(http://www.gob.mx/sedesol/documentos/criterios-

y-requisitos- para-ser- beneficiario-del- programa-

estancias-infantiles- 18688?state=published)</p>
  </li>
                    <li><p>Presentar copia y original de la identificación oficial vigente:</p><ul class="text-left listado">
                        <li><p>Credencial para votar</p></li>
                        <li><p>Cartilla del Servicio Militar Nacional</p></li>
                        <li><p>Pasaporte</p></li>
                        <li><p>Cédula Profesional</p></li>
                        <li><p>Cédula de identidad ciudadana</p></li>
                        <li><p>Credencial del INAPAM</p></li>
                        <li><p>Constancia de identidad de residencia con

fotografía</p></li>
                        <li><p>Formas Migratorias vigentes</p></li>
                        </ul></li>

                    <li><p>Proporcionar copia y original de Acta de

Nacimiento de la niña y niño, en su caso, copia y

original de escrito por parte de los padres o tutores

autorizando a una persona a realizar los trámites.</p>
  </li>
                                        <li><p>Presentar Clave Única de Registro de Población

(CURP) del solicitante y de las niñas y niños.</p>
  </li>
                                                            <li><p>En caso de que el menor sufra de alguna

discapacidad, presentar certificado médico que

indique la discapacidad, los medicamentos y los

cuidados.</p>
  </li>
                                                                                <li><p>Proporcionar información para el llenado del

Cuestionario Complementario del Programa.</p>
                    </li>
                 <li><p>Presentar un escrito en el cual el solicitante

declare que trabaja, estudia o está buscando

trabajo (el escrito deberá incluir nombre, domicilio

y nombre de los menores).</p>
                    </li>
                    <li><p>Demostrar estar en situación de pobreza.</p></li>

                </ul>
                                    <p><strong>NOTA</strong></p>
                    <p>Para cumplir con el apoyo, el solicitante no deberá tener

acceso a ningún servicio de cuidado infantil en

instituciones públicas u otros medios.</p>
                    <p>Personas que deseen establecer y operar una Estancia

Infantil</p>
                <ul class="text-left listado">
                    <li><p>Entregar solicitud de apoyo para afiliarse en las

oficinas de las delegaciones:

(http://www.gob.mx/sedesol/documentos/criterios-

y-requisitos- para-ser- beneficiario-del- programa-

estancias-infantiles- 18688?state=published).</p></li>
                    <li><p>Entregar original y copia de los siguientes

documentos</p><ul class="text-left listado"><li><p>Identificación oficial</p></li><li><p>Clave Única de Registro de Población

(CURP)</p></li><li><p>Registro Federal de Contribuyentes (RFC)</p></li><li><p>Estado de cuenta que incluya la clave

bancaria estandarizada (CLABE)</p></li><li><p>Entregar copia del comprobante de

domicilio del inmueble propuesto</p></li></ul></li>
                    <li><p>Presentar la certificación en el Estándar de

Competencia EC0435.</p></li>
                    <li><p>Presentar tres cartas de recomendación, sin

parentesco con la persona solicitante.</p></li>
                    <li><p>Ubicar el inmueble a no menos de 50 metros de

áreas que representen un alto riesgo.</p></li>
                    <li><p>Se deberá contar con un espacio físico para

atender a por lo menos 10 infantes (2 metros

cuadrados por infante).</p></li>
                    <li><p>Proporcionar información para llenado del

Cuestionario Complementario del Programa.</p></li>
                    <li><p>Entregar copia del documento que acredite la

propiedad del inmueble, en caso de arrendamiento

o comodato, se deberá incluir una cláusula que

indique la autorización del arrendador o

comodante para realizar adecuaciones en el

inmueble. El contrato deberá tener vigencia

mínima de 12 meses.</p></li>
                </ul>
                <div class="text-center">
                @include('partials.imgProgramas.pei.pei4',array())
                </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END TABS -->
    </div>

    @endsection
    @section('modals')
    @endsection
    @section('js-extras')
    @endsection