        @extends('layouts.programas')
        @include('partials/programas',array())
        @section('content')

        <div class="container">
            <div class="row head">
                <div class="col-md-12 ">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <img src="/assets/image/programasPng/paja.png" width="150px" height="150px">
                </div>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 text-right title_ins" >
                    <h1>PAJA</h1>
                        <p>Programa de Atención a Jornaleros Agrícolas</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row blank">
          <!-- TABS -->
          <div class="col-md-12 tab-style-1">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
              <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
              <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
                <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
            </ul>
            <div class="tab-content">
              <div id="tab-1" class="tab-pane row fade active in">
                <div class="col-md-12 text-center">
                  <p class="margin-bottom-10">Reducir las condiciones de precariedad de la población jornalera agrícola y de los integrantes de sus hogares.</p>
                </div>
                <div class="text-center">
                   @include('partials.imgProgramas.paja.paja1',array())
                </div>
              </div>
              <div id="tab-2" class="tab-pane row fade">
                <div class="col-md-12 text-left">
                  <p>Población jornalera agrícola integrada por mujeres y hombres de 16 años o más que laboran como jornaleras y jornaleros agrícolas, así como los integrantes de su hogar en situación de pobreza que tienen residencia o lugar de trabajo en las regiones de atención jornalera, ya sea de forma permanente o temporal.</p>
                </div>
                <div class="text-center">
                   @include('partials.imgProgramas.paja.paja2',array())
                </div>
              </div>
              <div id="tab-3" class="tab-pane fade">
                <div class="col-md-12 text-left">
                    <h3>Apoyos Directos a la Población Jornalera:</h3>
                        <ul class="text-left listado">
                            <li><p>Otorga un apoyo económico a los hogares jornaleros cuyos hijos e hijas menores de 18 años se encuentren estudiando. Otorga  montos desde 178 pesos hasta 669 pesos mensuales, dependiendo del grado escolar y edad del niño.
    </p></li>
                            <li><p> <strong> Apoyo Económico al Arribo: </strong></p><p>Se otorgará un apoyo económico de 800 pesos, por un máximo de 3 ocasiones al año al jefe o jefa del hogar jornalero que notifique su arribo en las Sedes de Atención del Programa. </p></li>
                        </ul>
                    <h3>Acciones para el Desarrollo de la Población Jornalera Agrícola:</h3>
                    <ul class="text-left listado">
                        <li>
                            <p><strong>Apoyos Alimenticios a las Niñas y Niños:</strong> 
Proporciona hasta dos alimentos diarios por una cantidad máxima de 480 pesos mensuales para niñas y niños.
                            </p>
                        </li>
                        <li>
                            <p>
                                <strong> Acciones de Protección Social y Participación Comunitaria:</strong>
 Son acciones para fortalecer las habilidades y competencias de la población jornalera agrícola, a través de:
                            </p>
                            <ul class="text-left listado">
                                <li><p>Promoción de los derechos humanos,</p></li>
                                <li><p>Educación para la protección contra la explotación y abuso sexual de los menores</p></li>
                                <li><p>Violencia de género</p></li>
                                <li><p>Migración</p></li>
                                <li><p>Hábitos saludables</p></li>
                                <li><p>Saneamiento básico</p></li>
                                <li><p>Contraloría social</p></li>
                                <li><p>Cuidado del medio ambiente, entre otros</p></li>
                            </ul>
                        </li>
                        <li><p><strong>Acciones para Potenciar el Desarrollo:</strong></p><p>Los integrantes del hogar jornalero podrán acceder a apoyos o servicios que brindan otros programas, siempre y cuando exista un acuerdo previo con las instituciones u organizaciones.</p></li>
                        <li><p><strong>Apoyos Especiales para Contingencias:</strong></p><p>Pago de transportación de regreso a su lugar de origen, servicios médicos y gastos funerarios para la o el jornalero agrícola migrante y sus acompañantes, en caso de percance. Este apoyo no sustituye las obligaciones del patrón respectivo si la contingencia ocurre durante la jornada laboral.</p></li>
                        <li><p><strong>Apoyos para Servicios Básicos:</strong></p><p>Subsidios para la construcción, rehabilitación, ampliación, acondicionamiento y equipamiento de vivienda para atender a la población jornalera en las Unidades de Trabajo. Se podrán ejecutar proyectos en los siguientes rubros:</p>
                        <ul class="text-left listado"><li><p>Desarrollo Infantil: Centros de atención y educación infantil, ludotecas, guarderías y estancias, entre otros.
                            <li><p>Atención a la salud: Unidades de atención médica de primer nivel fijas o móviles.</p></li>
                            <li><p>Espacios para la estancia temporal: Albergues y vivienda temporal, entre otros.</p></li>
                            <li><p>Instalaciones para la atención de la población jornalera agrícola: Sedes de atención.</p></li>
                            <li><p>De atención, alojamiento temporal e información, incluyendo Unidades de Servicios Integrales (USI) y Sedes de Atención permanentes, entre otros.</p></li>
</p></li></ul>
                        </li>
                    </ul>
                    <p>Además de los requisitos específicos de cada apoyo, un representante de cada hogar jornalero (preferentemente mujer de 16 años o más), a quien se entregarán los apoyos, que en su caso correspondan al hogar, deberá presentar algún documento que acredite su identidad.</p>
              </div>
                <div class="text-center">
                   @include('partials.imgProgramas.paja.paja3',array())
                </div>
              </div>
               <div id="tab-4" class="tab-pane row fade">
                <div class="col-md-12 text-left">
                    <h3>Apoyos Directos a la Población Jornalera Agrícola </h3>
                    <h4>Estímulos para la Asistencia y Permanencia Escolar</h4>
                    <ul class="text-left listado">
                        <li><p>Encontrarse en Unidades de Trabajo localizadas en Regiones de Atención Jornalera de destino.
</p></li>
                        <li><p>Tener menos de 18 años.
</p></li>
                        <li><p>Estar inscrito en una institución de educación preescolar, primaria, secundaria o equivalente.</p></li>
                    </ul>
                    <h4>Apoyo Económico al Arribo</h4>
                    <ul class="text-left listado">
                        <li><p>Ser integrante de un hogar jornalero agrícola.
</p></li>
                        <li><p>Arribar a Unidades de Trabajo localizadas en Regiones de Atención Jornalera.
</p></li>
                        <li><p>Registrarse en las Sedes de Atención del programa localizadas en las Regiones de Atención Jornalera de destino.</p></li>

                    <p>Además de los requisitos específicos de cada apoyo, un representante de cada hogar jornalero (preferentemente mujer de 16 años o más, se le entregarán los apoyos que en su caso correspondan al hogar, deberá de presentar alguno de los siguientes documentos que acredite su identidad:</p>

                    <lu class="listado">
                        <li><p>Credencial para votar vigente</p></li>
                        <li><p>Cartilla del Servicio Militar Nacional</p></li>
                        <li><p>Pasaporte</p></li>
                        <li><p>Cédula profesional</p></li>
                        <li><p>Credencial del INAPAM</p></li>
                        <li><p>
                            Registro familiar otorgado por el Programa de
                        Atención a Jornaleros Agrícolas
                        </p>
                        </li>
                        <li><p>Formas Migratorias</p></li>
                        <li><p>Cédula de Identidad Ciudadana</p></li>
                        <li><p>Cédula de Identidad Personal.</p></li>
                    </lu>
                    <li>
                        <p>
                            En localidades de hasta 10 mil habitantes, podrán
                            presentar Constancia de Identidad y Edad con fotografía,
                            expedida por autoridad municipal.
                        </p>
                    </li>
                    </ul>
                     <p>
                        Para acreditar sólo su edad:
                    </p>
                    <ul class="listado">
                        <li><p>CURP</p></li>
                        <li><p>Acta de nacimiento</p></li>
                    </ul>

                    <h4>Acciones para el Desarrollo de la Población Jornalera Agrícola</h4>
                    <p>Apoyos Alimenticios a las niñas y los niños</p>
                    <ul class="listado">
                        <li><p>Tener menos de 14 años</p></li>
                        <li><p>Encontrarse en Unidades de Trabajo</p></li>
                    </ul>
                    <br>
                    <p>Acciones de Protección Social y Participación Comunitaria y Acciones para Potenciar el Desarrollo:</p>
                    <ul class="listado">
                        <li><p>Ser integrante de un hogar jornalero agrícola.</p></li>
                        <li><p>
                            Encontrarse en Unidades de Trabajo con cobertura de la Red Social del programa.
                        </p></li>
                    </ul>
                    <br>
                    <p>Apoyos Especiales para Contingencias</p>
                    <ul class="listado">
                        <li><p>Ser integrante de un hogar jornalero agrícola.</p></li>
                        <li><p>
                            Encontrarse en Unidades de Trabajo con cobertura de la Red Social del programa.
                        </p></li>
                    </ul>
                    <br>
                    <p>Apoyos para los Servicios Básicos</p>
                    <ul class="listado">
                        <li><p>Presentar solicitud, mediante escrito libre que contenga:</p></li>
                        <ul class="listado">
                            <li><p>Nombre completo de la persona interesada, o persona
                            que acuda en su representación con firma y huella digital,
                            dirección de domicilio para recibir notificaciones, número
                            telefónico y correo electrónico.
                            </p></li>
                        </ul>
                        <li><p>Presentar un proyecto.</p></li>
                        <li><p>
                            Declaración de aceptación del compromiso de cumplir
                            con lo establecido en las Reglas de Operación del
                            Programa.
                        </p></li>
                        <li><p>
                            Firmar un convenio con la Sedesol, para asegurar que las
                            obras de apoyos para servicios básicos sean de uso
                            exclusivo de los jornaleros, por lo menos durante 10 años
                            a partir de su construcción.
                        </p></li>
                    </ul>
                </div>
                <div class="text-center">
                   @include('partials.imgProgramas.paja.paja4',array())
                </div>
              </div>
            </div>
          </div>
          <!-- END TABS -->
        </div>

        @endsection
        @section('modals')
        @endsection
        @section('js-extras')
        @endsection