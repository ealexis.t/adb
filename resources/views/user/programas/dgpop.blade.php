@extends('layouts.programas')
    @include('partials/programas',array())
@section('content')
<div class="container">
    <div class="row head">
        <div class="col-md-12 ">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              @include('partials.iconosProgramas.dgpop',array())

            </div>
            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 text-right title_ins" >
            <h1>PFES</h1>
                <p>PROGRAMA DE FOMENTO A LA ECONOMÍA SOCIAL</p>
            </div>
        </div>
    </div>
</div>
<div class="row blank">
  <!-- TABS -->
  <div class="col-md-12 tab-style-1">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
      <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
      <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
        <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
    </ul>
    <div class="tab-content">
      <div id="tab-1" class="tab-pane row fade active in">
        <div class="col-md-12 text-left">
          <p class="margin-bottom-10">Permite mejorar el ingreso de las personas en

situación de pobreza mediante el apoyo con

recursos económicos que permitan para

desarrollar o consolidar proyectos productivos y

fortalecer la organización, así como apoyos no

monetarios para capacitación y asistencia técnica.</p>
        </div>
        <div class="text-center">
          @include('partials.imgProgramas.pfes.pfes1',array())
        </div>
      </div>
      <div id="tab-2" class="tab-pane row fade">
        <div class="col-md-12 text-left">
          <p>A personas organizadas en: Sociedades

corporativas, ejidos, comunidades, organizaciones

de trabajadores, empresas que pertenezcan

mayoritaria o exclusivamente a los trabajadores; y

en general, de todas las formas de organización en

todo el país.</p>
            <p>A personas con ingresos por debajo de la línea de

bienestar, organizadas en grupos sociales (al

menos 3 personas) y que habitan en las siguientes

zonas de cobertura:</p>
            <ul class="text-left listado">
          <li><p> a) Las Zonas de Atención Prioritaria Rurales.</p></li>
          <li><p> b) Los municipios catalogados como

predominantemente indígenas.</p></li>
          <li><p>c) Las localidades de alta y muy alta marginación

con menos de 15,000 habitantes.</p></li>

          </ul>
        </div>
        <div class="text-center">
          @include('partials.imgProgramas.pfes.pfes2',array())
        </div>
      </div>
      <div id="tab-3" class="tab-pane fade">
        <div class="col-md-12 text-left">
            <h4>Apoyos para Impulso Productivo.</h4>
            <ul class="text-left listado">
          <li><p>Son apoyos económicos para poner en

marcha proyectos productivos. Con los

recursos se pueden comprar activos

(maquinarias, equipos, herramientas, etc.)

siempre y cuando éstos sean nuevos,

materias primas, insumos y servicios

necesarios para el proceso productivo. No

se puede pagar la mano de obra: sueldos o

jornales; obras de adaptación, remodelación

y/o construcción en terrenos públicos, ni

conceptos de asistencia técnica.</p></li>
          <li><p>Se podrá apoyar hasta con $40,000.00 por

integrante del grupo social y hasta con un

monto máximo de $320,000.00 por

proyecto.</p></li>
          <li><p>La aportación de los beneficiarios será del

5% al 20% del monto autorizado para el

Ficha: Programa de Fomento

proyecto, dependiendo de la zona de

cobertura donde se encuentre el proyecto y

de cómo se encuentre integrado el grupo

(por mujeres y hombres o sólo por mujeres).

a la Economía Social</p></li>
            </ul>
            <h4>Apoyos para el Desarrollo de Iniciativas

Productivas</h4>
                <ul>
          <li><p>Mediante estos apoyos se brinda asistencia

técnica y capacitación a los grupos sociales

que recibieron recursos para un proyecto

productivo, estos apoyos se otorgan a

través de Instituciones educativas.</p></li>
          </ul>
      </div>
      <div class="text-center">
          @include('partials.imgProgramas.pfes.pfes3',array())
        </div>
      </div>
       <div id="tab-4" class="tab-pane row fade">
        <div class="col-md-12 text-left">
            <h4>Apoyos para Impulso Productivo</h4>
            <ul class="text-left listado">
            <li><p>1. Estar registrado en el Sistema de

Focalización de Desarrollo (SIFODE).</p></li>
            <li><p>2. Contar con una idea de negocio o un

proyecto productivo operando que sea viable en su

municipio.</p></li>
            <li><p>3. Asistir a los talleres para mejorar su

proyecto productivo.</p></li>
                <li><p>4. Contar con los recursos monetarios o en

especie para cumplir con sus aportaciones.</p></li>
                <li><p>5. Obtener los permisos necesarios para llevar

a cabo su proyecto, en caso de que se requieran.</p></li>
                <li><p>6. Contar con Acta de Asamblea, donde se

señale quiénes forman el grupo social y quién es

su representante social.</p></li>

            </ul>
            <h4>Apoyos para el Desarrollo de Iniciativas

Productivas</h4>
            <ul class="text-left listado">
                <li><p>Ser una Institución de educación media

superior o superior que cuente con un área o

programa de incubación de proyectos, promotoría

y gestión o formación de emprendedores.</p></li>
                <li><p>Estar dado de alta como contribuyente ante

la SHCP y estar al corriente en sus obligaciones

fiscales.</p></li>
                <li><p>Demostrar capacidad y experiencia para

realizar el proceso de formulación de proyectos,

promotoría y gestión o formación de

Ficha: Programa de Fomento

emprendedores.</p></li>
                <li><p>Contar con una propuesta técnica y

económica para el Desarrollo de Iniciativas

Productivas.</p></li>
            </ul>
        </div>
        <div class="text-center">
          @include('partials.imgProgramas.pfes.pfes4',array())
        </div>
      </div>
    </div>
  </div>
  <!-- END TABS -->
</div>

@endsection
@section('modals')
@endsection
@section('js-extras')
@endsection