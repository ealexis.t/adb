    @extends('layouts.programas')
    @include('partials/programas',array())
    @section('content')

    <div class="container">
        <div class="row head">
            <div class="col-md-12 ">
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                  <img src="/assets/image/programasPng/pet.png" width="150px" height="150px">
                </div>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 text-right title_ins" >
                <h1>PET</h1>
                    <p>Programa de Empleo Temporal</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row blank">
      <!-- TABS -->
      <div class="col-md-12 tab-style-1">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#tab-1">Función</a></li>
          <li class=""><a data-toggle="tab" href="#tab-2">¿A quiénes apoya?</a></li>
          <li class=""><a data-toggle="tab" href="#tab-3">¿Cómo apoya?</a></li>
            <li class=""><a data-toggle="tab" href="#tab-4">¿Cómo recibir el apoyo?</a></li>
        </ul>
        <div class="tab-content">
          <div id="tab-1" class="tab-pane row fade active in">
            <div class="col-md-12 text-left">
              <p class="margin-bottom-10">Dotar de esquemas de seguridad social que

protejan el bienestar socioeconómico de la

población en situación de carencia o pobreza,

mediante la mitigación del impacto económico y

social de las personas de 16 años de edad o más

que ven disminuidos sus ingresos o patrimonio

ocasionado por situaciones económicas y sociales

adversas, emergencias o desastres.</p>

            </div>
            <div class="text-center">
              @include('partials.imgProgramas.pet.pet1',array())
            </div>
          </div>
          <div id="tab-2" class="tab-pane row fade">
            <div class="col-md-12 text-left">
              <p>Mujeres y hombres de 16 años de edad en

adelante que ven afectado su patrimonio o

enfrentan una disminución temporal en su ingreso

por baja demanda de mano de obra o por los

efectos de situaciones sociales y económicas

adversas, emergencias o desastres.</p>
                
            </div>
            <div class="text-center">
              @include('partials.imgProgramas.pet.pet2',array())
            </div>
          </div>
          <div id="tab-3" class="tab-pane fade">
            <div class="col-md-12 text-left">
                <p>Para madres trabajadoras, que busquen empleo o estudien; y padres solos</p>
                <ul class="text-left listado">
              <li><p>Apoyos directos:</p>
                  
<ul class="text-justify listado"><li><p>Apoyo económico a la beneficiaria o

beneficiario consistente en jornales

(salarios) equivalentes al 99% de un

salario mínimo general diario vigente.</p></li>
                  
                  
    <li><p>Se podrán otorgar apoyos

económicos para la adquisición o

arrendamiento de materiales,

herramientas, maquinaria o equipo,

incluyendo implementos de protección

para las y los beneficiarios, así como

costos de transporte necesarios para

realizar los proyectos autorizados.</p></li></ul>
                    
                    
                    
                    </li>
              <li><p>Proyectos para el Mejoramiento del Entorno Físico e Infraestructura Comunitaria:</p>
            <ul class="text-justify listado"><li><p>Apoyo económico mediante al pago

de jornales (salarios), priorizando los

proyectos que generen impacto

directo a las y los habitantes de una

localidad determinada e

indirectamente beneficien su calidad

de vida, en función de los criterios

para la definición de los indicadores

de pobreza establecidos por el

CONEVAL, priorizando las acciones

relacionadas con las dimensiones de

calidad, espacios y servicios básicos,

todo ello en la vivienda; así como de

infraestructura social comunitaria.</p></li>
</ul>        
            </li>

                    
                  
                    
              </ul>
                
          </div>
            <div class="text-center">
              @include('partials.imgProgramas.pet.pet3',array())
            </div>
          </div>
           <div id="tab-4" class="tab-pane row fade">
            <div class="col-md-12 text-justify">
                <ul class="text-left listado">
                <li><p>Tener 16 años de edad o más.</p>
  </li>
                    <li><p>Presentar copia y original de la identificación oficial vigente:</p><ul class="text-left listado">
                        <li><p>Credencial para votar vigente</p></li>
                        <li><p>Cartilla del Servicio Militar Nacional</p></li>
                        <li><p>Pasaporte vigente</p></li>
                        <li><p>Cédula Profesional</p></li>
                        <li><p>Cédula de identidad ciudadana</p></li>
                        <li><p>Credencial del INAPAM</p></li>
                        <li><p>Constancia de Identidad y Edad con fotografía, expedida por la autoridad municipal; en localidades de hasta 10 mil habitantes.</p></li>
                        <li><p>Formas Migratorias vigentes</p></li>
                        </ul></li>

                    <li><p>Para realizar un proyecto de mejoramiento al entorno físico</p>
                        <ul class="text-left listado">
                        <li><p>Presentar solicitud de apoyo en escrito libre señalando el tipo de apoyo importancia de la obra y acciones a
 realizar e indicadores de la carencia a abatir original, la solicitud deberá contener:</p>
                        <ul class="text-left listado"> 
                            <li><p>Presupuesto detallado, personas a beneficiar desagregadas por género y edad, metas a alcanzar (jornales y
 acciones), permisos y autorizaciones en caso de ser necesarias</p></li>
                        </ul>
  </li>
                        </ul>
  </li>
                        <li><p>Para realizar un proyecto</p>
                            <ul class="text-left listado"> 
                            <li><p>Presentar original y copia de una solicitud para la ejecución de un proyecto, que deberá contener:</p>
                            <ul class="text-left listado"> 
                                <li><p>Nombre del municipio y de la localidad en donde se realizará el proyecto.</p>
                                </li>
                                <li><p>Descripción breve del proyecto.</p></li>
                                <li><p>Nombre completo del interesado o la persona que representa a los solicitantes y su firma o huella.</p></li>
                            </ul>
                            </li>
                        </ul>
                        </li>
                                                                                
                </ul>
            </div>
            <div class="text-center">
              @include('partials.imgProgramas.pet.pet4',array())
            </div>
          </div>
        </div>
      </div>
      <!-- END TABS -->
    </div>

    @endsection
    @section('modals')
    @endsection
    @section('js-extras')
    @endsection