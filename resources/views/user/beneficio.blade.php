 @extends('layouts.app')
    <link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/normalize.css">
    <link rel="stylesheet" href="/assets/css/main.css">
    <link rel="stylesheet" href="/assets/css/jquery.steps.css">
    <style>
        .wizard > .actions {
            margin-right:80px;
            top:-300px;
        }
    </style>
    <style>
    body
    {
            background: url("/assets/image/ppam.png")  fixed center !important;
    }
    .tresd
    {
    -moz-box-shadow: 0 0 5px 5px #AD0056;
    -webkit-box-shadow: 0 0 5px 5px #AD0056;
    box-shadow: 0 0 5px 5px #AD0056;
            border-radius:50px !important;
    }
    .back-header
    {
      background-color:#AD0056;
    }
    .black-head
        {
            background: rgba(0,0,0,0.5);
            padding-bottom:20px;
            position:relative;
            top:-55px;
        }
    .top10
        {
            margin-top:10px;
        }
            .top30
        {
            margin-top:30px;
        }
    h1{
        font-size: 18px !important;
    }
    h2{
        font-size: 18px !important;
    }
img.img-responsive2
        {
            width:100% !important;
            height:80px !important;
        }
        .button {
      background: #3498db;
      background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
      background-image: -moz-linear-gradient(top, #3498db, #2980b9);
      background-image: -ms-linear-gradient(top, #3498db, #2980b9);
      background-image: -o-linear-gradient(top, #3498db, #2980b9);
      background-image: linear-gradient(to bottom, #3498db, #2980b9);
      -webkit-border-radius: 28;
      -moz-border-radius: 28;
      border-radius: 28px;
      -webkit-box-shadow: 1px 2px 3px #9e9e9e;
      -moz-box-shadow: 1px 2px 3px #9e9e9e;
      box-shadow: 1px 2px 3px #9e9e9e;
      font-family: Arial;
      color: #ffffff;
      font-size: 25px;
      padding: 10px 20px 10px 20px;
      text-decoration: none;
    }

    .button:hover {
      background: #3cb0fd;
      background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
      background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
      background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
      background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
      background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
      text-decoration: none;
    }
        .selected
        {
            -webkit-box-shadow: 1px 1px 13px #6f0cf0;
      -moz-box-shadow: 1px 1px 13px #6f0cf0;
      box-shadow: 1px 1px 13px #6f0cf0;
              background: #3cb0fd;
      background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
      background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
      background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
      background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
      background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
      text-decoration: none;
        }

    ul li{
            text-decoration: none;
        }

    </style>


    @include('partials/top',array())
    @section('content')

        <div class="row black-head" >
            <div class="col-md-12 col-xs-12 title_ins2 text-center">
            <h1>¿QUÉ BENEFICIOS PUEDO TENER?</h1>
                <br>
            </div>
        </div>
    <div class="row ">
        <input type="hidden" name="genero" id="genero" value="" />
        <input type="hidden" name="lactancia" id="lactancia" value="" />
        <input type="hidden" name="edad" id="edad" value="" />
        <input type="hidden" name="civil" id="civil" value="" />
        <input type="hidden" name="laboras" id="laboras" value="" />
        <input type="hidden" name="discapacidad" id="discapacidad" value="" />
        <input type="hidden" name="hijos"  id="hijos" value="" />
        <input type="hidden" name="grupoedades1"  id="grupoedades1" value="" />
        <input type="hidden" name="grupoedades2"  id="grupoedades2" value="" />
        <input type="hidden" name="grupoedades3"  id="grupoedades3" value="" />
        <input type="hidden" name="comunidad" id="comunidad" value="" />
        <input type="hidden" name="grupo" id="grupo" value="" />
    <div class="col-md-12 col-xs-12 back blank " style="margin-bottom:120px; padding-top:20px;">
        <div class="content">
            {{Form::open(array('name' => 'beneficio', 'url' => '/Beneficios/respuesta', 'method' => 'post', 'class'=>'',))}}
                <div id="wizard">
                    <h2>SELECCIONA EL SEXO AL QUE PERTENECES</h2>
                    <section>
                        <h4 class="text-center">SELECCIONA EL SEXO AL QUE PERTENECES</h4>
                        <div class="col-md-12 text-center">
                            <div class="question1">
                                <div class="top10 col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xs-offset-2 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">

                                    <h1>HOMBRE</h1>
                                        <div class="button" id="req1">
                                            @include('partials.beneficio.pregunta1.hombre',array())
                                        </div>

                                </div>
                                <div class=" top10 col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <h1>MUJER</h1>
                                    <span>
                                        <div class="button" id="req2">
                                            @include('partials.beneficio.pregunta1.mujer',array())
                                        </div>
                                    </span>

                                </div>
                                                    </div>
                            <div class="col-md-12 text-center">
                     <div  class="lactancia">
                            <section>
                                <h5>¿ESTÁS EN PERÍODO DE LACTANCIA O GESTACIÓN?</h5>
                                <div class="img">
                                    <div class="col-md-6">
                                        <div class="button" id="req3">
                                            <h6>SI</h6>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="button" id="req4">
                                            <h6>NO</h6>
                                        </div>
                                    </div>
                                </div>
                            </section>
                                </div>
                            </div>
                        </div>
                    </section>

                    <h2>¿CUÁL ES TU EDAD?</h2>
                    <section>
                        <h4 class="text-center">¿CUÁL ES TU EDAD?</h4>
                        <div class="row">
                            <div class="col-md-12">
                                                    <div class="img">
                            @include('partials.beneficio.pregunta2.edad',array())
                        </div>
                            <select id="tuedad" name="tuedad" class="form-control">
                                <option value="">Selecciona una opción</option>
                            </select>
                            </div>
                        </div>

                    </section>

                    <h2>SELECCIONA TU ESTADO CIVIL</h2>
                    <section>
                        <div class="row">
                             <div class="col-lg-12 col-md-12 col-xs-12 ">
                                    <h4 class="text-center">SELECCIONA TU ESTADO CIVIL</h4>
                             </div>
                        </div>
                        <div class="row top10">


                        <div class="img col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
                            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                            <h2 class="text-center margin-bottom-10 soltero">Solter@</h2>
                            <div class="button" id="req5">
                                @include('partials.beneficio.pregunta3.soltero',array())
                            </div>

                        </div>
                        <div class="img col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                            <h2 class="text-center margin-bottom-10 casado">Casad@</h2>
                            <div class="button" id="req6">
                                @include('partials.beneficio.pregunta3.casado',array())
                            </div>

                        </div>
                        <div class="img col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                                    <h2 class="text-center margin-bottom-10">Union libre</h2>
                            <div class="button" id="req7">
                                @include('partials.beneficio.pregunta3.unionlibre',array())
                            </div>
                        </div>
                             </div>
                    </section>

                     <h2>¿TIENES ALGÚN TIPO DE DISCAPACIDAD?</h2>
                    <section>
                        <div class="row">
                             <div class="col-lg-12 col-md-12 col-xs-12 ">
                                    <h4 class="text-center">¿TIENES ALGÚN TIPO DE DISCAPACIDAD?</h4>
                             </div>
                        </div>
                        <div class="row top10">

                            <div class="img col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                        <div class="button" id="req18">
                                            <h6>SI</h6>
                                        </div>

                        </div>
                        <div class="img col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                        <div class="button" id="req19">
                                            <h6>NO</h6>
                                        </div>
                        </div>
                             </div>
                    </section>

                    <h2>SELECCIONA TU OFICIO O PROFESIÓN</h2>
                    <section>
                        <div class="row">
                             <div class="col-lg-12 col-md-12 col-xs-12 ">
                                 <h4 class="text-center">SELECCIONA TU OFICIO O PROFESIÓN</h4>
                             </div>
                        </div>
                        <div class="row top10">


                        <div class="img col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                        <div class="button" id="req8">
                                            <h6 class="jornalero">JORNALERO AGRÍCOLA</h6>
                                        </div>

                        </div>
                        <div class="img col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                        <div class="button" id="req9">
                                            <h6 class="artesano">ARTESANO</h6>
                                        </div>
                        </div>
                        </div>
                            <div class="row top10">
                            <div class="img col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                        <div class="button" id="req10">
                                            <h6>OTRO</h6>
                                        </div>
                             </div>
                            <div class="img col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                        <div class="button" id="req11">
                                            <h6>NO TENGO TRABAJO ACTUALMENTE</h6>
                                        </div>
                             </div>
                        </div>
                    </section>
                    <!--
                    <h2>¿TIENES HIJOS O ERES RESPONSABLE LEGAL DE ALGÚN MENOR DE EDAD?</h2>
                    <section>

                       <div class="row">
                             <div class="col-lg-12 col-md-12 col-xs-12 ">
                                 <h4 class="text-center">¿TIENES HIJOS O ERES RESPONSABLE LEGAL DE ALGÚN MENOR DE EDAD?</h4>
                             </div>
                        </div>
                        <div class="row top10">
                        <div class="img col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="button" id="req12">
                                            <h6>SI</h6>
                                        </div>

                        </div>
                        <div class="img col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="button" id="req13">
                                            <h6>NO</h6>
                                        </div>
                        </div>
                        </div>

                        <div class="edades">
                                              <div class="row top30 margin-bottom10">
                             <div class="col-lg-12 col-md-12 col-xs-12 ">
                                 <h4 class="text-center">SELECCIONA EL O LOS GRUPOS DE EDADES A LOS QUE PERTENECEN</h4>
                             </div>
                        </div>
                        <div class="row top10">
                        <div class="img col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <div class="button" id="req20">
                                            <h6>0 A 3</h6>
                                        </div>

                        </div>
                        <div class="img col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <div class="button" id="req21">
                                            <h6>4 A 12</h6>
                                        </div>
                        </div>
                           <div class="img col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <div class="button" id="req22">
                                            <h6>13 A 23</h6>
                                        </div>
                        </div>
                         <div class="img col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                        <div class="button" id="req23">
                                            <h6>24 A 29</h6>
                                        </div>
                        </div>
                        </div>
                        </div>
                    </section>
-->
                    <h2>SELECCIONA EL TIPO DE ZONA DONDE VIVES</h2>
                    <section>
                         <div class="row">
                             <div class="col-lg-12 col-md-12 col-xs-12 ">
                                 <h4 class="text-center">SELECCIONA EL TIPO DE ZONA DONDE VIVES</h4>
                             </div>
                        </div>
                        <div class="row top10">
                            <div class="img col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                <h2 class="text-center margin-bottom-10">RURAL</h2>
                                <div class="button" id="req14">
                                    @include('partials.beneficio.pregunta6.rural',array())
                                </div>

                            </div>
                            <div class="img col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                <h2 class="text-center margin-bottom-10">URBANA</h2>
                                <div class="button" id="req15">
                                    @include('partials.beneficio.pregunta6.urbana',array())
                                </div>
                            </div>
                        </div>
                    </section>
                    <h2>¿CONSIDERAS QUE VIVEN MÁS DE 15000 HABITANTES EN TU COMUNIDAD?</h2>
                    <section>
                        <div class="row">
                             <div class="col-lg-12 col-md-12 col-xs-12 ">
                                 <h4 class="text-center">¿CONSIDERAS QUE VIVEN MÁS DE 15000 HABITANTES EN TU COMUNIDAD?</h4>
                             </div>
                        </div>
                        <div class="row top10">
                        <div class="img col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                        <div class="button" id="req16">
                                            <h6>SI</h6>
                                        </div>

                        </div>
                        <div class="img col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                        <div class="button" id="req17">
                                            <h6>NO</h6>
                                        </div>
                        </div>
                        <div class="img col-xs-4 col-sm-4 col-md-4 col-lg-4">
                            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                        <div class="button" id="req24">
                                            <h6>NO SÉ</h6>
                                        </div>
                        </div>
                        </div>
                    </section>
                    <h2>VER RESULTADOS</h2>
                    <section>
                        <div class="row">
                             <div class="col-lg-12 col-md-12 col-xs-12 ">
                                 <h4 class="text-center">¿QUIERES VER LOS RESULTADOS?</h4>
                             </div>
                        </div>
                        <div class="row top10">
                        <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                            <!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                        <div class="button" id="req23">
                                            <h6>SI</h6>
                                        </div>

                            </div>
                        </div>
                        <div class="row top10">
                        <div class="resultados2">
                            <h2>Los beneficios a los que puedes acceder son:</h2>
                        </div>
                        </div>
                        <div class="row top10">
                        <div class="resultados">
                        </div>
                        </div>
                        <div class="row top10 imprimir">
                            <div class="col-lg-12 col-md-12 col-xs-12 text-center">
                            <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal">
							 IMPRIMIR INFORMACIÓN
						  </button>
                                 </div>
                        </div>

                    </section>

                </div>
            {{ Form::close() }}
        </div>
    </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		{{Form::open(array('name' => 'pdf', 'url' => 'pdf', 'method' => 'post', 'class'=>'',))}}
			<div class="modal-content">
			  <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    <h4 class="modal-title uppercase" id="myModalLabel">LLENA EL FORMULARIO PARA IMPRIMIR TU FICHA INFORMATIVA</h4>
			  </div>
			  <div class="modal-body">

			    <div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="icon-equalizer font-red-sunglo"></i>
							<span class="caption-subject font-red-sunglo bold uppercase"></span>
						</div>
						<div class="actions">
						</div>
					</div>
					<div class="portlet-body form">
						<!-- BEGIN FORM-->
						<div class="form-body">
                            <div class="form-group">
                                <label class=" control-label">Los resultados son únicamente de carácter informativo y no comprometen a la SEDESOL a otorgar el beneficio</label>
								<label class=" control-label">Este programa es público, ajeno a cualquier partido político. Queda prohibido el uso para fines distintos a los establecidos en el programa.</label>
							</div>
							<div class="form-group">
								<label class=" control-label">Nombre*</label>
								<div class="">
									<div class="input-icon">
										<i class="fa fa-user"></i>
										<input type="text" name="nombre" class="form-control" placeholder="Nombre">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class=" control-label">Teléfono</label>
								<div class="">
									<div class="input-icon">
										<i class="fa fa-bell-o"></i>
										<input type="text" name="telefono" class="form-control" placeholder="Teléfono">
									</div>
								</div>
							</div>
                            <div class="form-group">
								<label class=" control-label">Celular</label>
								<div class="">
									<div class="input-icon">
										<i class="fa fa-bell-o"></i>
										<input type="text" name="celular" class="form-control" placeholder="Celular">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class=" control-label">Correo electrónico</label>
								<div class="">
									<div class="input-icon">
										<i class="fa fa-bell-o"></i>
										<input type="text" name="email" class="form-control" placeholder="Correo electrónico">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class=" control-label">Código Postal</label>
								<div class="">
									<div class="input-icon">
										<i class="fa fa-bell-o"></i>
										<input type="text" name="cp" class="form-control" placeholder="Código Postal">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class=" control-label">Si no conoces tu "Código Postal" coloca tu dirección aquí</label>
								<div class="">
									<div class="input-icon">
										<i class="fa fa-bell-o"></i>
										<input type="text" name="direccion" class="form-control" placeholder="Si no conoces tu Codigo Postal coloca tu dirección aquí">
									</div>
								</div>
							</div>
                            <div class="form-group">


                                <label class=" control-label"> Yo, (nombre completo), declaro que es de mi conocimiento que la afiliación a los programas de los cuales puedo ser candidato, dependen de la disponibilidad y el cumplimiento de los requisitos establecidos en las reglas de operación de    dichos programas</label>
                                <input type="checkbox" name="compromiso" placeholder=""  style="width:15px;"/> <span>Aceptar</span>

                            </div>

						</div>
						<div class="form-actions">

						</div>
						<!-- END FORM-->
					</div>
				</div>
			  </div>
			  <div class="modal-footer">

                <input type='hidden' id="resProspera" name='resProspera' value='0'/>
                <input type='hidden' id="resInapam" name='resInapam' value='0'/>
                <input type='hidden' id="resLiconsa" name='resLiconsa' value='0'/>
                <input type='hidden' id="resPpam" name='resPpam' value='0'/>
                <input type='hidden' id="resPei" name='resPei' value='0'/>
                <input type='hidden' id="resDiconsa" name='resDiconsa' value='0'/>
                <input type='hidden' id="resImjuve" name='resImjuve' value='0'/>
                <input type='hidden' id="resPet" name='resPet' value='0'/>
                <input type='hidden' id="resFonart" name='resFonart' value='0'/>
                <input type='hidden' id="resPaja" name='resPaja' value='0'/>
                <input type='hidden' id="resPaimef" name='resPaimef' value='0'/>
                <input type='hidden' id="res3x1" name='res3x1' value='0'/>
                <input type='hidden' id="resComedores" name='resComedores' value='0'/>
                <input type='hidden' id="resConadis" name='resConadis' value='0'/>


			    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			    <button type="submit" class="btn btn-primary">IMPRIMIR</button>
			  </div>
			</div>
		{{ Form::close() }}
	</div>
</div>
    @section('modals')

    @endsection

    @section('js-extras')
    <script src="/assets/js/modernizr-2.6.2.min.js"></script>
    <script src="/assets/js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="/assets/js/jquery.cookie-1.3.1.js"></script>
    <script src="/assets/js/jquery.steps.js"></script>
    <script>
    /*
    $( ".req2" ).on( "click", function() {

      alert('funciona');
      console.log('funciona');
    });
    */


    $(function (){
        var maxima=115;
        var html='<option value="">Selecciona una opción</option>';
        for(var i=0; i <= maxima; i++ )
        {
           html= html + '<option value="'+i+'">'+i+'</option>';
        }
        $("#tuedad").html(html);
        $(".lactancia").hide();
        $(".edades").hide();
        $("#wizard").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            stepsOrientation: "vertical"
        });
        $(".resultados1").hide();
        $(".resultados2").hide();
        $(".imprimir").hide();
    });
    function razones(array)
    {

    }
    function compress(p)
    {
        //data = arr.join(",");
        var prof1=/[AG]/;
        var prof0=/[AGD]/;
        var prof2=/[AD]/;
        //SEPARAMOS A DE GyD
        /*
        if(prof0.test(p)==true)
        {
            var resultado=p.replace(/[AGD]/i,"");
            var grupo1= "A,"+resultado;
            var grupo2= "G,D,"+resultado;
            var solucion= [$.unique(grupo1.split(',')),
            $.unique(grupo2.split(','))];
            return solucion;

        }
        //SEPARAMOS A de G
        else if(prof1.test(p)==true)
        {
            var resultado=p.replace(/[AG]/i,"");
            var grupo1= "A,"+resultado;
            var grupo2= "G,"+resultado;
            var solucion= [$.unique(grupo1.split(',')),
            $.unique(grupo2.split(','))];
            return solucion;
        }
        */
        //SEPARAMOS A de D
         if(prof1.test(p)==true)
        {
            var resultado=p.replace(/[AD]/i,"");
            var grupo1= "A,"+resultado;
            var grupo2= "D,"+resultado;
            var solucion= [$.unique(grupo1.split(',')),
            $.unique(grupo2.split(','))];
            return solucion;
        }
        else
        {
            return [$.unique(p.split(','))];
        }

    }
    function abasto(p,s)
    {
        if(p==1 && (s==1 || s==2) )
        {
         return "F,";
        }
        return "";
    }
    function laboras(p)
    {
        if(p==0)
        {
            return "K,";
        }
        else if(p==1)
        {
            return "J,";
        }
        else if(p==2)
        {
             return "";
        }
        else if(p==3)
        {
            return "";
        }
    }
    function discapacidad(p)
    {
       if(p==1)
        {
            return "C,O,P,";
        }
        return "";
    }

    function evaluaedad(p,s)
    {
        if(p >= 60)
        {
         return "A,B,C,I,";
        }
        else if(p >= 65)
        {
          return "A,B,C,O,I,D," ;
        }
        else if( p >= 12 && p<=29)
        {
          var respuesta="H,";

            if(p <= 19)
                {
                    respuesta = respuesta+"O,";
                    if(p>=16)
                    {
                       return respuesta= respuesta + "I,";
                    }
                    else if(p >= 13 && p<=15 && s==0)
                    {
                       return respuesta= respuesta + "C,";
                    }
                    else if(p==12)
                    {
                        return respuesta= respuesta + "C,";
                    }
                    return respuesta;
                }

            return respuesta;
        }
        else if( p >= 0 && p<=11)
        {
            var respuesta="O,";
            if(p >= 0 && p<=4)
            {
                return respuesta= respuesta + "E,C,A,";
            }
            else
            {
                return respuesta= respuesta + "C,A,";
            }
        }
        else if(p>=16)
        {
            return  "I,";
        }
        else
            {
                return "";
            }



    }

    function razon(a)
    {
        $('#resProspera').val('0');
        $('#resInapam').val('0');
        $('#resLiconsa').val('0');
        $('#resPpam').val('0');
        $('#resPei').val('0');
        $('#resDiconsa').val('0');
        $('#resImjuve').val('0');
        $('#resPet').val('0');
        $('#resFonart').val('0');
        $('#resPaja').val('0');
        $('#resPaimef').val('0');
        $('#res3x1').val('0');
        $('#resComedores').val('0');
        $('#resConadis').val('0');

        var html="";
        var res="";
        for(var i=0;i < a.length; i++)
        {
            console.log(a[i]);
            if(a[i]=="A")
            {
                html = html + "<li>PROSPERA</li>";
                $('#resProspera').val('1');
            }
            else if(a[i]=="B")
                {
                    html = html + "<li>INAPAM</li>";
                    $('#resInapam').val('1');

                }
                        else if(a[i]=="C")
                {
                    html = html + "<li>LICONSA</li>";
                    $('#resLiconsa').val('1');
                }
                        else if(a[i]=="D")
                {
                    html = html + "<li>PPAM</li>";
                    $('#resPpam').val('1');
                }
                        else if(a[i]=="E")
                {
                    html = html + "<li>PEI</li>";
                    $('#resPei').val('1');
                }
                        else if(a[i]=="F")
                {
                    html = html + "<li>DICONSA</li>";
                    $('#resDiconsa').val('1');
                }
            /*
                        else if(a[i]=="G")
                {
                    html = html + "<div class=\" col-md-3 col-xs-3 col-lg-3 \">PAL</div>";
                }
                */
                        else if(a[i]=="H")
                {
                    html = html + "<li>IMJUVE</li>";
                    $('#resImjuve').val('1');
                }
                        else if(a[i]=="I")
                {
                    html = html + "<li>PET</li>";
                    $('#resPet').val('1');
                }
                        else if(a[i]=="J")
                {
                    html = html + "<li>FONART</li>";
                    $('#resFonart').val('1');
                }
                        else if(a[i]=="K")
                {
                    html = html + "<li>PAJA</li>";
                    $('#resPaja').val('1');
                }
                                    else if(a[i]=="M")
                {
                    html = html + "<li>PAIMEF</li>";
                    $('#resPaimef').val('1');
                }
                                    else if(a[i]=="N")
                {
                    html = html + "<li>3 x 1</li>";
                    $('#res3x1').val('1');
                }
                                    else if(a[i]=="O")
                {
                    html = html + "<li>COMEDORES COMUNITARIOS</li>";
                    $('#resComedores').val('1');
                }
                                    else if(a[i]=="P")
                {
                    html = html + "<li>CONADIS</li>";
                    $('#resConadis').val('1');
                }
        }
        return html;
    }
    function evalua(a)
    {
        console.log(a.length);
        if(a.length==2)
        {

            $(".resultados").html('<div class=\"col-xs-6 col-md-6 col-lg-6\">OPCIÓN 1</div><div class=\"col-xs-6 col-md-6 col-lg-6\">OPCIÓN 2</div><div class=\"col-xs-6 col-md-6 col-lg-6\"><ul class=\"list-unstyled\">'+razon(a[0])+'</ul></div><div class=\"col-xs-6 col-md-6 col-lg-6\"><ul class=\"list-unstyled\">'+razon(a[1])+'</ul></div>');
        }
        else{
            return razon(a[0]);
        }
    }

    $(document).on("click","#req23",function()
    {
        var sexo=$("#genero").val();
        var edad=$("#tuedad").val();
        var disc=$("#discapacidad").val();
        var labo=$("#laboras").val();
        var comu=$("#comunidad").val();
        var grupo=$("#grupo").val();
        var lactancia="";
        var respuesta="";
        var eval="";

        $(".resultados1").show();
                $(".resultados2").show();
        $(".imprimir").show();
        if(sexo=="0")
        {
            respuesta= respuesta + "M,";
            lactancia=$("#lactancia").val();

            if(lactancia=="1")
            {
                respuesta= respuesta + "G,O,C,";
                respuesta= respuesta + evaluaedad(edad,sexo);
                respuesta= respuesta + discapacidad(disc);
                respuesta= respuesta + laboras(labo);
                respuesta= respuesta + abasto(comu,grupo);
                respuesta=compress(respuesta);
                eval=evalua(respuesta);
                console.log(eval);
                $(".resultados").html(eval);
            }
            else
            {
                respuesta= respuesta + evaluaedad(edad,sexo);
                respuesta= respuesta + discapacidad(disc);
                respuesta= respuesta + laboras(labo);
                respuesta= respuesta + abasto(comu,grupo);
                respuesta=compress(respuesta);
                eval=evalua(respuesta);
                $(".resultados").html(eval);
            }
        }
        else  if(sexo=="1")
        {
                if(edad!="")
                    {
                      respuesta= respuesta + evaluaedad(edad,sexo);
                        respuesta= respuesta + discapacidad(disc);
                        respuesta= respuesta + laboras(labo);
                        respuesta= respuesta + abasto(comu,grupo);
                        respuesta=compress(respuesta);
                        eval=evalua(respuesta);
                        $(".resultados").html(eval);
                    }

        }
    });


    $(document).on("click","#req2",function()
    {
            $(this).addClass("selected");
            $("#req1").removeClass("selected");
            $(".lactancia").show();
            $("#genero").val("0");
            $(".jornalero").text("JORNALERA");
            $(".artesano").text("ARTESANA");
            $(".soltero").text("SOLTERA");
            $(".casado").text("CASADA");
    });
    $(document).on("click","#req1",function()
    {
            $(this).addClass("selected");
            $("#req2").removeClass("selected");
            $(".lactancia").hide();
            $("#lactancia").val("N");
            $("#genero").val("1");
            $(".jornalero").text("JORNALERO");
            $(".artesano").text("ARTESANO");
            $(".soltero").text("SOLTERO");
            $(".casado").text("CASADO");
    });

    $(document).on("click","#req3",function()
    {
            $(this).addClass("selected");
            $("#req4").removeClass("selected");
            $("#lactancia").val("1");
    });
    $(document).on("click","#req4",function()
    {
            $(this).addClass("selected");
            $("#req3").removeClass("selected");
            $("#lactancia").val("0");
    });

    $(document).on("click","#req5",function()
    {
            $(this).addClass("selected");
            $("#req6").removeClass("selected");
            $("#req7").removeClass("selected");
            $("#civil").val("0");
    });
    $(document).on("click","#req6",function()
    {
            $(this).addClass("selected");
            $("#req5").removeClass("selected");
            $("#req7").removeClass("selected");
            $("#civil").val("1");
    });
    $(document).on("click","#req7",function()
    {
            $(this).addClass("selected");
            $("#req5").removeClass("selected");
            $("#req6").removeClass("selected");
            $("#civil").val("2");
    });

    $(document).on("click","#req8",function()
    {
            $(this).addClass("selected");
            $("#req9").removeClass("selected");
            $("#req10").removeClass("selected");
            $("#req11").removeClass("selected");
            $("#laboras").val("0");
    });
    $(document).on("click","#req9",function()
    {
            $(this).addClass("selected");
            $("#req8").removeClass("selected");
            $("#req10").removeClass("selected");
            $("#req11").removeClass("selected");
            $("#laboras").val("1");
    });

        $(document).on("click","#req10",function()
    {
            $(this).addClass("selected");
            $("#req8").removeClass("selected");
            $("#req9").removeClass("selected");
            $("#req11").removeClass("selected");
            $("#laboras").val("2");
    });
    $(document).on("click","#req11",function()
    {
            $(this).addClass("selected");
            $("#req8").removeClass("selected");
            $("#req9").removeClass("selected");
            $("#req10").removeClass("selected");
            $("#laboras").val("3");
    });

        $(document).on("click","#req12",function()
    {
            $(this).addClass("selected");
            $("#req13").removeClass("selected");
            $(".edades").show();
            $("#hijos").val("1");
    });
        $(document).on("click","#req13",function()
    {
            $(this).addClass("selected");
            $("#req12").removeClass("selected");
            $(".edades").hide();
            $("#gruposedades").val("N");
            $("#hijos").val("0");
    });


        $(document).on("click","#req14",function()
    {
            $(this).addClass("selected");
            $("#req15").removeClass("selected");
            $("#comunidad").val("1");
    });
        $(document).on("click","#req15",function()
    {
            $(this).addClass("selected");
            $("#req14").removeClass("selected");
            $("#comunidad").val("0");
    });

            $(document).on("click","#req16",function()
    {
            $(this).addClass("selected");
            $("#req17").removeClass("selected");
                            $("#req24").removeClass("selected");
            $("#grupo").val("1");
    });
        $(document).on("click","#req17",function()
    {
            $(this).addClass("selected");
            $("#req16").removeClass("selected");
            $("#req24").removeClass("selected");
            $("#grupo").val("0");
    });
                $(document).on("click","#req24",function()
    {
            $(this).addClass("selected");
            $("#req16").removeClass("selected");
            $("#req17").removeClass("selected");
            $("#grupo").val("0");
    });



            $(document).on("click","#req18",function()
    {
            $(this).addClass("selected");
            $("#req19").removeClass("selected");
            $("#discapacidad").val("1");
    });
        $(document).on("click","#req19",function()
    {
            $(this).addClass("selected");
            $("#req18").removeClass("selected");
            $("#discapacidad").val("0");
    });



    $(document).on("click","#req20",function()
    {
            if ($(this).hasClass("selected"))
                {
                    $(this).removeClass("selected");
                    $("#grupoedades1").val("0");
                }
                else{
                    $(this).addClass("selected");
                    $("#grupoedades1").val("1");
                }
    });
    $(document).on("click","#req21",function()
    {
             if ($(this).hasClass("selected"))
                {
                    $(this).removeClass("selected");
                    $("#grupoedades2").val("0");
                }
                else{
                    $(this).addClass("selected");
                    $("#grupoedades2").val("1");
                }
    });
    $(document).on("click","#req22",function()
    {
                if ($(this).hasClass("selected"))
                {
                    $(this).removeClass("selected");
                    $("#grupoedades3").val("0");
                }
                else{
                    $(this).addClass("selected");
                    $("#grupoedades3").val("1");
                }
    });


    </script>
    @endsection

    @endsection