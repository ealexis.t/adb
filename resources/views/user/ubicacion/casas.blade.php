    @extends('layouts.app')
    @section('style')
     <style>
     #map { min-height: 400px; width:100%; }
     body
      {
    background: url("/assets/image/ppam.png")  fixed center !important;
      }
      .back{
        background: #fff;
        padding: 40px;
        padding-bottom: 20px;
      }
      .back-header
      {
        background-color:#AD0056;
      }
      .black-head
      {
          background: rgba(0,0,0,0.5);
          padding-bottom:20px;
          position:relative;
          top:-55px;
      }
    </style>

    @endsection
    @include('partials/donde',array())
    @section('content')
    <div class="container">
    <div class="row black-head">
          <div class="col-md-12 col-xs-12 text-center title_ins2">
              <h1 >CASAS ARTESANALES</h1>
          </div>
    </div>
    </div>

   <div class="container back">
      <div class="col-md-12">
    
    </div>    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-md-offset-2">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="title_place2">
                    <h1>Datos de la ubicación</h1>
                </div>
            </div>
            <div class="row item_collect">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="title_place">
                    <h1>Dirección:</h1>
                                    <p class="direccion">Francisco P. Castañeda 107, Universidad, 50130 Toluca de Lerdo, Méx., México</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="title_place">
                    <h1>Responsable:</h1>
                    <p class="responsable">Edmundo Rafal Ranero Barrera</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="title_place">
                    <h1>Teléfonos de contacto:</h1>
                    <p class="telefono">722 212 2296</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="title_place">
                    <h1>Horarios:</h1>
                    <p class="horarios">Sin registro</p>
                </div>
            </div>

                    </div>
                                </div>
    <div class="col-lg-12">
    <!--
    <label>Selecciona la distancia, DISTANCIA ACTUAL: <span id="distancia">500</span> metros de distancia</label>
    <input type=range min=1 max=20 value=1 step=1 id="range">
    -->
    </div>
      <div class="col-md-12">
      <div id="map"></div>
      </div>
   </div>
        <input type="hidden" name="ep" id="ep">

    @endsection
    @section('modals')

    @endsection
    @section('js-extras')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
     <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script type="text/javascript">
    var markers= []; var poly, geodesicPoly;
    var map;


    function initMap() {

               var center = {
        url: '/assets/image/kiosco.png',
        // This marker is 20 pixels wide by 32 pixels high.
        scaledSize: new google.maps.Size(40, 40),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(0, 40)
      };


      map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 19.2781257, lng: -99.6503582},
        zoom: 12
      });
                  var minZoomLevel = 12;
                 google.maps.event.addListener(map, 'zoom_changed', function () {
     if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
 });

                                          var item = {
        url: '/assets/image/estancias.png',
        // This marker is 20 pixels wide by 32 pixels high.
        scaledSize: new google.maps.Size(40, 40),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(0, 40)
      };
        
      markers[0] = new google.maps.Marker({
        map: map,
        icon: center,
        position: {lat: 19.2781257, lng: -99.6503582}
      });
        markers[0].addListener('click', function()
                              {
            $(".direccion").text("Francisco P. Castañeda 107, Universidad, 50130 Toluca de Lerdo, Méx., México");
            $(".responsable").text("Edmundo Rafal Ranero Barrera");
            $(".telefono").text("722 212 2296");
            $(".horarios").text("Sin registro");
        });
    /*
      poly = new google.maps.Polyline({
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 3,
        map: map,
      });

      geodesicPoly = new google.maps.Polyline({
        strokeColor: '#CC0099',
        strokeOpacity: 1.0,
        strokeWeight: 3,
        geodesic: true,
        map: map
      });
    */
      //update();
    }

    function update() {

      var path = [markers[0].getPosition(), markers[1].getPosition()];
        /*
      poly.setPath(path);
      geodesicPoly.setPath(path);
      */
      var distance = google.maps.geometry.spherical.computeDistanceBetween(path[0],path[1]);
      var heading = google.maps.geometry.spherical.computeHeading(path[0], path[1]);
      document.getElementById('heading').value = heading;
      document.getElementById('origin').value = path[0].toString();
      document.getElementById('destination').value = path[1].toString();
      document.getElementById('distance').value = distance;
    }
    $(document).on("change","#range",function()
    {
        var metros=$("#range").val()/2 * 1000;
        $("#distancia").text(metros);
        var path = [markers[0].getPosition(),markers[1].getPosition(),markers[2].getPosition(),markers[3].getPosition(),markers[4].getPosition(),markers[5].getPosition(),markers[6].getPosition()];
        for(var i=1; i < path.length; i++)
        {
            distance = google.maps.geometry.spherical.computeDistanceBetween(path[0],path[i]);

            if(distance > metros)
            {
                //console.log("i:"+i+" se va");
               markers[i].setVisible(false);
            }
            else{
                //console.log("i:"+i+" se queda "+distance);
                 markers[i].setVisible(true);
            }
        }
    });
        </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBn5blIIcG0LCtdS9uslDnpGHSamFPDeUI&libraries=geometry&callback=initMap"></script>
    @endsection