@extends('layouts.app')
<style>
body
{
       background: url("/assets/image/ppam.png")  fixed center !important;
}
.tresd
{
-moz-box-shadow: 0 0 5px 5px #AD0056;
-webkit-box-shadow: 0 0 5px 5px #AD0056;
box-shadow: 0 0 5px 5px #AD0056;
        border-radius:50px !important;
}
.back-header
{
  background-color:#AD0056;
}
.black-head
    {
        background: rgba(0,0,0,0.5);
        padding-bottom:20px;
        position:relative;
        top:-55px;
    }
</style>
@include('partials/top',array())
@section('content')

    <div class="row black-head">
        <div class="col-md-12 col-xs-12 title_ins2 text-center">
            <h1>BUZÓN DE SUGERENCIAS</h1>
        </div>
    </div>
<div class="row blank">

</div>
<div class="row blank">
    <div class="col-md-4">
        @include('partials.buzon.buzon',array())
    </div>
    <div class="col-md-6">
        <h4>Rellene el formulario con sus datos para ponernos en contacto con usted </h4>
        <form>
            <div class="form-group">
                <label>Nombre:</label>
                <input type="text" name="" class="form-control">
            </div>
            <div class="form-group">
                <label>Correo electrónico:</label>
                <input type="text" name="" class="form-control">
            </div>
            <div class="form-group">
                <label>Teléfono:</label>
                <input type="text" name="" class="form-control">
            </div>
            <div class="form-group">
                <label>Asunto:</label>
                <textarea class="form-control" rows="3"></textarea>
            </div>
            <div class="form-group">
                <input type="submit" value="Enviar" class="btn btn-block btn-success">
            </div>
        </form>
    </div>
</div>

@endsection
@section('modals')

@endsection
@section('js-extras')

@endsection