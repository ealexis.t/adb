@extends('layouts.app')

@section('css-extras')
<link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
  <!-- Page level plugin styles END -->

  <!-- Theme styles START -->
  <link href="/assets/global/css/components.css" rel="stylesheet">
  <link href="/assets/frontend/layout/css/style.css" rel="stylesheet">
  <link href="/assets/frontend/layout/css/style-responsive.css" rel="stylesheet">
  <link href="/assets/frontend/layout/css/themes/red.css" rel="stylesheet" id="style-color">
  <link href="/assets/frontend/layout/css/custom.css" rel="stylesheet">
<style>
body
{
        background: url("/assets/image/ppam.png")  fixed center !important;
}
.tresd
{
-moz-box-shadow: 0 0 5px 5px #AD0056;
-webkit-box-shadow: 0 0 5px 5px #AD0056;
box-shadow: 0 0 5px 5px #AD0056;
        border-radius:50px !important;
}
.back-header
{
  background-color:#AD0056;
}
.black-head
    {
        background: rgba(0,0,0,0.5);
        padding-top:20px;
        padding-bottom:20px;
        position:relative;
        top:-55px;

    }
</style>
@endsection
@include('partials/top',array())
@section('content')

<div class="row black-head">
    <div class="col-md-12 col-xs-12 title_ins2 text-center">
        <h1 >NOTICIAS</h1>
    </div>
</div>

    <div class="main " >
    <br>
        <!-- BEGIN SIDEBAR & CONTENT -->
        <div class="row margin-bottom-40" >
          <!-- BEGIN CONTENT -->
          <div class="col-md-12 col-sm-12" >
            <h1 ></h1>
            <div class="content-page">
              <div class="row">
                <!-- BEGIN SERVICE BLOCKS -->
                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 ">
                  <div class="row margin-bottom-20">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div class="service-box-v1">
                        <div><img src="/assets/image/noticias/imjuve.jpg" alt="" class="img-responsive"></div>
                        <h2 style="font-size:18px;">Llama Meade Kuribreña a jóvenes del país a transformar y construir soluciones para México</h2>
                        <p></p>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div class="service-box-v1">
                        <div><img src="/assets/image/noticias/legisladores.jpg" alt="" class="img-responsive"></div>
                        <h2 style="font-size:18px;">Meade Kuribreña sostuvo reuniones de trabajo con legisladores y autoridades capitalinas </h2>
                        <p></p>
                      </div>
                    </div>
                  </div>
                  <div class="row margin-bottom-20">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div class="service-box-v1">
                        <div><img src="/assets/image/noticias/liconsa.jpg" alt="" class="img-responsive"></div>
                        <h2 style="font-size:18px;">Todo un éxito el programa piloto que vende a un peso la leche en 150 municipios pobres del país: Meade Kuribreña</h2>
                        <p></p>
                      </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <div class="service-box-v1">
                        <div><img src="/assets/image/noticias/alimentacion1.jpg" alt="" class="img-responsive"></div>
                        <h2 style="font-size:18px;">Sedesol concluye en Campeche, la Jornada Nacional de Alimentación en todo el país</h2>
                        <p></p>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- END SERVICE BLOCKS -->

                <!-- BEGIN VIDEO AND TESTIMONIALS -->
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                  <!-- BEGIN VIDEO -->
                  <!-- END VIDEO -->
                     <video width="100%" height="400" controls autoplay loop>
  <source src="/assets/image/noticias/noticiavideo.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 
                <!-- BEGIN TESTIMONIALS -->
               
                <!-- END TESTIMONIALS -->
                </div>
                <!-- END BEGIN VIDEO AND TESTIMONIALS -->
              </div>



            </div>
          </div>
        </div>
      </div>

@endsection
@section('modals')

@endsection
@section('js-extras')

@endsection