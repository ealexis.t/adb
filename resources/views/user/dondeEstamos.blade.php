@extends('layouts.app')
<style>
body
{
    background: url("/assets/image/ppam.png")  fixed center !important;
}
.tresd
{
-moz-box-shadow: 0 0 5px 5px #AD0056;
-webkit-box-shadow: 0 0 5px 5px #AD0056;
box-shadow: 0 0 5px 5px #AD0056;
        border-radius:50px !important;
}
.back-header
{
  background-color:#AD0056;
}
.black-head
    {
        background: rgba(0,0,0,0.5);
        padding-bottom:20px;
        position:relative;
        top:-55px;
    }
</style>
@include('partials/top',array())
@section('content')
<div class="row black-head">
    <div class="col-md-12 col-xs-12 title_ins2 text-center">
        <h1 >¿DÓNDE ESTAMOS?</h1>
    </div>
</div>
<div class="row blank">
      <div class="team-block content content-center margin-bottom-40" id="team">
    <div class="col-md-12 col-xs-12">
        <a href="{{ URL('Donde-Estamos/delegaciones') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    @include('partials.iconosDonde.delegaciones',array())
				</div>
				<h3>DELEGACIONES</h3>
	        </div>
        </a>
       <a href="{{ URL('Donde-Estamos/fonart') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    @include('partials.iconosDonde.fonart',array())
				</div>
				<h3>TIENDAS FONART</h3>
	        </div>
        </a>


         <a href="{{ URL('Donde-Estamos/pei') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    @include('partials.iconosDonde.pei',array())
				</div>
				<h3>ESTANCIAS INFANTILES</h3>
	        </div>
        </a>


         <a href="{{ URL('Donde-Estamos/diconsa') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    @include('partials.iconosDonde.diconsa',array())
				</div>
				<h3>TIENDAS DICONSA (EN CONSTRUCCIÓN)</h3>
	        </div>
        </a>

    </div>
    </div>
</div>


<div class="row blank">
      <div class="team-block content content-center margin-bottom-40" id="team">


    <div class="col-md-12 col-xs-12">
                                   <a href="{{ URL('Donde-Estamos/imjuve') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    @include('partials.iconosDonde.imjuve',array())
				</div>
                    <h3>CENTROS PODER JOVEN</h3>
	        </div>
        </a>
        <a href="{{ URL('Donde-Estamos/liconsa') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    @include('partials.iconosDonde.liconsa',array())
				</div>
				<h3>LECHERÍAS LICONSA (EN CONSTRUCCIÓN)</h3>
	        </div>
        </a>

       <a href="{{ URL('Donde-Estamos/prospera') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    @include('partials.iconosDonde.prospera',array())
				</div>
				<h3>DELEGACIONES PROSPERA (EN CONSTRUCCIÓN)</h3>
	        </div>
        </a>




         <a href="{{ URL('Donde-Estamos/inapam') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    @include('partials.iconosDonde.clubes',array())
				</div>
				<h3>CLUBES INAPAM (EN CONSTRUCCIÓN)</h3>
	        </div>
        </a>


    </div>
    </div>
</div>

<div class="row blank">
      <div class="team-block content content-center margin-bottom-40" id="team">
    <div class="col-md-12 col-xs-12">
        <a href="{{ URL('Donde-Estamos/paimef') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                 @include('partials.iconosDonde.instancias',array())        
				</div>
				<h3>CENTROS DE APOYO A MUJERES EN ENTIDADES FEDERATIVAS </h3>
	        </div>
        </a>
       <a href="{{ URL('Donde-Estamos/organizaciones') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">
                    @include('partials.iconosDonde.osc',array())
				</div>
				<h3>ORGANIZACIONES DE LA SOCIEDAD CIVIL (EN CONSTRUCCIÓN)</h3>
	        </div>
        </a>


         <a href="{{ URL('Donde-Estamos/casas') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">  
                @include('partials.iconosDonde.casas',array())
				</div>
                    <h3>CASAS ARTESANALES (EN CONSTRUCCIÓN)</h3>
	        </div>
        </a>


         <a href="{{ URL('Donde-Estamos/comedores') }}">
	        <div class="col-lg-3 col-sm-3 col-xs-3 col-md-3 item">
				<div class="img">  
                     @include('partials.iconosDonde.comedores',array())
				</div>
				<h3>COMEDORES COMUNITARIOS (EN CONSTRUCCIÓN)</h3>
	        </div>
        </a>

    </div>
    </div>
</div>
-->

  <!-- Team block END -->
@endsection
@section('modals')

@endsection
@section('js-extras')

@endsection