@extends('layouts.app')
@section('content')

<script src="/assets/js/jquery.js"></script>
<script src="/assets/js/jquery.dataTables.js"></script>
<link rel="stylesheet" href="/assets/css/jquery.dataTables.min.css"/>
<div class="col-md-12">
    <a href="#" class="btn btn-sm bg-green-haze" data-toggle="modal" data-dismiss="modal"  data-target="#addUser">Agregar Usuario</a>
</div>
<br><br>
<table id="foo" class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Usuario</th>
            <th>Nombre</th>
            <th>Rol</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{$user->username}}</td>
            <td>{{$user->nombre}} {{$user->apPaterno}} {{$user->apMaterno}}</td>
            <td>
            @foreach($roles as $rol)
                @if($user->rol == $rol->id)
                    {{$rol->name}}
                @endif
            @endforeach
            </td>
            <td><a href="#" class="btn btn-md red-flamingo" data-toggle="modal" data-dismiss="modal"  data-target="#editPass{{$user->id}}">Editar Pass</a></td>
            <td><a href="#" class="btn btn-md green-jungle" data-toggle="modal" data-dismiss="modal"  data-target="#editUser{{$user->id}}">Editar Usuario</a></td>
        </tr>

        <?php
            /*
            |-------------------------------------------------------------
            | Modal Editar Usuario
            |----------------------------------------------------
            */
         ?>
        <div class="modal fade" id="editUser{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                {{ Form::open(array('name' => 'f2','url' => 'updateUser',  'method' => 'put','class'=>'form-horizontal row-fluid'))}}
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Editar Usuario</h4>
                        </div>
                        <div class="modal-body text-justify">
                            <hr>
                            <h4>Datos de Acceso</h4>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Username:</label>
                                <div class="col-md-10">
                                    <div class="input-icon">
                                        <i class="fa fa-user"></i>
                                        <input name="username" type="text" class="form-control" value="{{$user->username}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Correo:</label>
                                <div class="col-md-10">
                                    <div class="input-icon">
                                        <i class="fa fa-envelope-o"></i>
                                        <input name="email" type="text" class="form-control" value="{{$user->email}}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Tipo de Usuario:</label>
                                <div class="col-md-10">
                                    <div class="input-icon">
                                        <i class="fa fa-user"></i>
                                        <input name="rol" class="form-control" type="text" value="{{$user->rol}}">
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <h4>Datos de Personales</h4>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Nombre:</label>
                                <div class="col-md-10">
                                    <div class="input-icon">
                                        <i class="fa fa-bell-o"></i>
                                        <input name="nombre" type="text" class="form-control" value="{{$user->nombre}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Apellido Paterno:</label>
                                <div class="col-md-10">
                                    <div class="input-icon">
                                        <i class="fa fa-bell-o"></i>
                                        <input name="apPaterno" type="text" class="form-control" value="{{$user->apPaterno}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Apellido Materno:</label>
                                <div class="col-md-10">
                                    <div class="input-icon">
                                        <i class="fa fa-bell-o"></i>
                                        <input name="apMaterno" type="text" class="form-control" value="{{$user->apMaterno}}">
                                    </div>
                                </div>
                            </div>
                            <br><br><br><br><br><br><br><br><br>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="id" value="{{$user->id}}">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn green-haze" onclick="this.disabled=true;this.form.submit();">Actualizar</button>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
        <?php
            /*
            |-------------------------------------------------------------
            | Modal Editar Password
            |----------------------------------------------------
            */
         ?>
        <div class="modal fade" id="editPass{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                 {{ Form::open(array('name' => 'f3','url' => 'updatePass',  'method' => 'put','class'=>'form-horizontal row-fluid'))}}
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Editar Contraseña</h4>
                        </div>
                        <div class="modal-body text-justify">
                            <h4>Datos de Acceso</h4>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Username:</label>
                                <div class="col-md-10">
                                    <div class="input-icon">
                                        <i class="fa fa-user"></i>
                                        <input name="username" type="text" class="form-control" placeholder="{{$user->username}}" disabled>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Contraseña:</label>
                                <div class="col-md-10">
                                    <div class="input-icon">
                                        <i class="fa fa-lock"></i>
                                        <input name="password" type="text" class="form-control" placeholder="New Pass" required='true'>
                                    </div>
                                </div>
                            </div>
                            <br><br><br><br>

                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="id" value="{{$user->id}}">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn green-haze" onclick="this.disabled=true;this.form.submit();">Actualizar</button>
                        </div>

                    </div>
                {{ Form::close() }}
            </div>
        </div>
        @endforeach
    </tbody>
</table>

<?php
    /*
    |-------------------------------------------------------------
    | Modal Agregar Usuario
    |----------------------------------------------------
    */
 ?>
<div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
            {{ Form::open(array('name' => 'f5','url' => 'addUser',  'method' => 'post','class'=>'form-horizontal row-fluid'))}}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Agregra Usuario</h4>
                </div>
                <div class="modal-body text-justify">
                    <hr>
                    <h4>Datos de Acceso</h4>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Username:</label>
                        <div class="col-md-10">
                            <div class="input-icon">
                                <i class="fa fa-user"></i>
                                <input name="username" type="text" class="form-control" placeholder="Nombre de Usuario">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Contraseña:</label>
                        <div class="col-md-10">
                            <div class="input-icon">
                                <i class="fa fa-lock"></i>
                                <input name="password" type="text" class="form-control" placeholder="Password">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Correo:</label>
                        <div class="col-md-10">
                            <div class="input-icon">
                                <i class="fa fa-envelope-o"></i>
                                <input name="email" type="text" class="form-control" placeholder="Correo">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Tipo de Usuario:</label>
                        <div class="col-md-10">
                            <select name="rol" class="form-control">
                                <option value="">Seleccionar</option>
                                <option value="3">User</option>
                                <option value="2">Editor</option>
                                <option value="1">Administrador</option>
                            </select>
                        </div>
                    </div>

                    <hr>
                    <h4>Datos de Personales</h4>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Nombre:</label>
                        <div class="col-md-10">
                            <div class="input-icon">
                                <i class="fa fa-bell-o"></i>
                                <input name="nombre" type="text" class="form-control" placeholder="Nombre">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Apellido Paterno:</label>
                        <div class="col-md-10">
                            <div class="input-icon">
                                <i class="fa fa-bell-o"></i>
                                <input name="apPaterno" type="text" class="form-control" placeholder="Apellido Paterno">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Apellido Materno:</label>
                        <div class="col-md-10">
                            <div class="input-icon">
                                <i class="fa fa-bell-o"></i>
                                <input name="apMaterno" type="text" class="form-control" placeholder="Apellido Materno">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn green-haze" >Agregar</button>
                </div>

            </div>
        {{ Form::close() }}
    </div>
</div>
<script type="text/javascript">
/*
**Ordena alfabéticamente de manera ascendente la tabla usuarios tomando la primera columna como referencia
*/
$("#foo").dataTable({
    order: [[1, "desc"]]
});
</script>
@endsection
@section('js-extras')

@endsection