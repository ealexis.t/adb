<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    //Use table own
    protected $table = 'roles';

    protected $fillable = [
    	'id',
        'name',
    ];
}