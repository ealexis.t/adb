<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;

use DB;

class AdminController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');

        $this->users = DB::table('users')->get();
        $this->roles = DB::table('roles')->get();

        view()->share('users', $this->users);
        view()->share('roles', $this->roles);
    }

    public function dashboard()
    {
        if(Auth::user()->idRol == '1'){


            return view('admin.dashboard');
        }
        else{
            return view('404');
        }
    }


    public function users()
    {
        if(Auth::user()->idRol == '1'){

            return view('admin.users')
            			->with('users', $this->users);
        }
        else{
            return view('404');
        }
    }


    public function store(Request $request)
    {

         if(Auth::user()->idRol == '1'){
        	$user = new User;
        	$user->username = $request->username;
        	$user->email = $request->email;
        	$user->password = bcrypt($request->password);
        	$user->nombre = $request->nombre;
        	$user->apMaterno = $request->apMaterno;
        	$user->apMaterno = $request->apMaterno;
        	$user->idEntidades = $request->idEntidad;
        	$user->idCarencia = $request->idCarencia;
        	$user->rol = $request->rol;
        	$user->save();

            return redirect()->back()->with('message', 'Usuario Agregado');
        }
        else{
            return view('404');
        }
    }


    public function updatePass(Request $request)
    {

       $pass = bcrypt($request->password);

        if(Auth::user()->idRol == '1'){

            DB::table('users')
            ->where('id', $request->input('id'))
            ->update([
                'password' => $pass,
                ]);

            return redirect()->back()->with('message', 'Contraseña Actualizada');
        }
        else{
            return view('404');
        }
    }


    public function updateUser(Request $request)
    {
        if(Auth::user()->idRol == '1'){

            DB::table('users')
            ->where('id', $request->input('id'))
            ->update([
                'username' => $request->username,
                'email' => $request->email,
                'nombre' => $request->nombre,
                'apMaterno' => $request->apMaterno,
                'apPaterno' => $request->apPaterno,
                'idCarencia' => $request->idCarencia,
                'rol' => $request->rol,
                ]);

            return redirect()->back()->with('message', 'Información Actualizada');
        }
        else{
            return view('404');
        }
    }

}
