<?php

namespace App\Http\Controllers;

#use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProgramasController extends Controller
{
    public function paimef(){

        return view('user.programas.paimef');
    }
    public function conadis(){

        return view('user.programas.conadis');
    }
    public function imjuve(){

        return view('user.programas.imjuve');
    }
    public function inaes(){

        return view('user.programas.inaes');
    }
    public function fonart(){

        return view('user.programas.fonart');
    }
    public function diconsa(){

        return view('user.programas.diconsa');
    }
        public function liconsa(){

        return view('user.programas.liconsa');
    }
        public function prospera(){

        return view('user.programas.prospera');
    }
    public function indesol(){

    return view('user.programas.indesol');
    }
            public function inapam(){

        return view('user.programas.inapam');
    }
                public function pei(){

        return view('user.programas.pei');
    }
          public function dgpop(){

        return view('user.programas.dgpop');
    }
     public function svjf(){

        return view('user.programas.svjf');
    }
         public function ppam(){

        return view('user.programas.ppam');
    }
        public function paja(){

        return view('user.programas.paja');
    }
         public function pet(){

        return view('user.programas.pet');
    }
      public function tres(){

        return view('user.programas.tres');
    }
          public function comedores(){

        return view('user.programas.comedores');
    }

    public function coinversion(){
        return view('user.programas.coinversion');
    }
}