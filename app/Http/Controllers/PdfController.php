<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Beneficiario;
use DB;

class PdfController extends Controller
{
    public function invoice(Request $request)
    {
        /*
        |------------------------------------------------------------------
        | Resultado de beneficios
        |-----------------------------------------------
        */
        $resProspera = $request->resProspera;
        $resInapam = $request->resInapam;
        $resLiconsa = $request->resLiconsa;
        $resPpam = $request->resPpam;
        $resPei = $request->resPei;
        $resDiconsa = $request->resDiconsa;
        $resImjuve = $request->resImjuve;
        $resPet = $request->resPet;
        $resFonart = $request->resFonart;
        $resPaja = $request->resPaja;
        $resPaimef = $request->resPaimef;
        $res3x1 = $request->res3x1;
        $resComedores = $request->resComedores;
        $resConadis = $request->resConadis;
        $resCompromiso = $request->compromiso;

        if($resCompromiso == 'on'){
                     /*
            |-----------------------------------------------------------------
            |  Datos Beneficiario
            |----------------------------------------------------------
            */
            if($request->direccion != null){
                $direccion = $request->direccion;
            }
            if($request->direccion == null){
                $direccion = 0;
            }

            if($request->telefono != null){
                $telefono = $request->telefono;
            }
            if($request->telefono == null){
                $telefono = 0;
            }


            if($request->celular != null){
               $celular = $request->celular;
            }
            if($request->celular == null){
               $celular = 0;
            }

            if($request->nombre != null){
                $nombre = $request->nombre;
            }
            if($request->nombre == null){
                $nombre = 0;
            }

            if($request->email != null){
               $email = $request->email;
            }
            if($request->email == null){
               $email = 0;
            }

            if($request->cp != null){
               $cp = $request->cp;
            }
            if($request->cp == null){
               $cp = 0;
            }

            /*
            |-----------------------------------------------------------------
            |  Generar Folio
            |----------------------------------------------------------
            */
            $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            $numerodeletras=10;
            $folio = "";
            for($i=0;$i<$numerodeletras;$i++)
            {
                $folio .= substr($caracteres,rand(0,strlen($caracteres)),1);
            }

            if($request->nombre != null){
                $beneficiario = new Beneficiario;
                $beneficiario->folio = $folio;
                $beneficiario->nombre = $nombre;
                $beneficiario->telefono = $telefono;
                $beneficiario->celular = $celular;
                $beneficiario->email = $email;
                $beneficiario->cp = $cp;
                $beneficiario->direccion = $direccion;
                $beneficiario->resProspera = $resProspera;
                $beneficiario->resInapam = $resInapam;
                $beneficiario->resLiconsa = $resLiconsa;
                $beneficiario->resPpam = $resPpam;
                $beneficiario->resPei = $resPei;
                $beneficiario->resDiconsa = $resDiconsa;
                $beneficiario->resImjuve = $resImjuve;
                $beneficiario->resPet = $resPet;
                $beneficiario->resFonart = $resFonart;
                $beneficiario->resPaja = $resPaja;
                $beneficiario->resPaimef = $resPaimef;
                $beneficiario->res3x1 = $res3x1;
                $beneficiario->resComedores = $resComedores;
                $beneficiario->resConadis = $resConadis;
                $beneficiario->save();
            }

            $data = $this->getData();
            $date = date('Y-m-d');
            $invoice = $folio;
            $view = \View::make('user.pdf.invoice',
                        compact(
                            'data',
                            'date',
                            'invoice',
                            'nombre',
                            'telefono',
                            'resProspera',
                            'resInapam',
                            'resLiconsa',
                            'resPpam',
                            'resPei',
                            'resDiconsa',
                            'resImjuve',
                            'resPet',
                            'resFonart',
                            'resPaja',
                            'resPaimef',
                            'res3x1',
                            'resComedores',
                            'resConadis'
                        ))->render();

            $pdf = \App::make('dompdf.wrapper');
            #$pdf->set_paper("A4", "landscape");
            $pdf->loadHTML($view);
            return $pdf->stream('invoice');

        }
        if($resCompromiso != 'on'){
            return redirect()->back();
        }

    }

    public function getData()
    {
        $data =
        [
            'identidad'=>'Credencial para votar vigente, constancia de identidad con fotografía o de residencia, Pasaporte, Cartilla del Servicio Militar Nacional, Credencial del INAPAM, Cédula de Identidad Ciudadana, Cédula de Identidad Personal (sólo para menores de 18 años), Licencia de conducir, Credencial con fotografía de servicios médicos de una institución pública, Credencial con fotografía de jubilados(as) o pensionado(a)',
            'migratorios'=>'FM2 (Documento Migratorio de Inmigrante), FM3 (Documento Migratorio de No Inmigrante), Forma Migratoria de Inmigrante, Forma Migratoria de No Inmigrante, Forma Migratoria de Inmigrado',

            'PROSPERA' =>
            [
            'description'=>'Buscar que las familias que viven en situación de pobreza mejoren su calidad de vida, a través de acciones que amplíen sus capacidades en alimentación, salud y educación, y mejoren su acceso a otras dimensiones de bienestar.',
            'documentos'=>'Acta de nacimiento, CURP, Cualquiera de los documentos migratorios enlistados anteriormente'
            ],
            'PROSPERA' =>
            [
            'description'=>'Contribuir a la construcción de una sociedad incluyente mediante políticas que fomenten el ejercicio de los derechos de las personas adultas mayores y apoyar el desarrollo humano integral, a fin de mejorar sus niveles de bienestar y calidad de vida.',
            'documentos'=>'Acta de nacimiento, CURP, Cualquiera de los documentos migratorios enlistados anteriormente'
            ],
        ];



        return $data;
    }
}

