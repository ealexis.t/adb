<?php

namespace App\Http\Controllers;

#use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Referencia;

class UbicaController extends Controller
{

    public function gsp(Request $request)
    {
        $resultado=Referencia::where("institucion_id",$request->id)->where("estado","=","15")->where("revision","=","L")->get();
        return response()->json(['data'=>$resultado]);
    }
        public function getdata(Request $request)
    {
        $resultado=Referencia::where("id",$request->id)->get();
        return response()->json(['data'=>$resultado]);
    }
    public function actualiza(){

        return view('user.ubicacion.actualiza');
    }
    public function descarga(Request $request)
    {
        $resultado=Referencia::where("institucion_id","2")->where("revision","=","R")->where("estado",$request->id)->get();
        return response()->json(['data'=>$resultado]);
    }
     public function carga(Request $request)
     {
        $resultado=Referencia::where("id",$request->id)->first();
        $resultado->longitud=$request->longitud;
        $resultado->latitud=$request->latitud;
        $resultado->revision="L";
        $resultado->save();
    }
    
    public function tiendasfonart(){

        return view('user.ubicacion.fonart');
    }
        public function liconsa(){

        return view('user.ubicacion.liconsa');
    }
            public function inapam(){

        return view('user.ubicacion.inapam');
    }
            public function prospera(){

        return view('user.ubicacion.prospera');
    }
            public function organizaciones(){

        return view('user.ubicacion.organizaciones');
    }
                public function comedores(){

        return view('user.ubicacion.comedores');
    }
                public function casas(){

        return view('user.ubicacion.casas');
    }
                public function paimef(){

        return view('user.ubicacion.paimef');
    }
        public function tiendasdiconsa(){

        return view('user.ubicacion.diconsa');
    }
            public function pei(){

        return view('user.ubicacion.pei');
    }
    public function delegaciones(){
        return view('user.ubicacion.delegaciones');
    }
    public function imjuve(){
        return view('user.ubicacion.imjuve');
    }

}