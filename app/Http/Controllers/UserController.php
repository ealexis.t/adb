<?php

namespace App\Http\Controllers;

#use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Beneficiario;

class UserController extends Controller
{
    public function getfolio(Request $request){
        $resultado=Beneficiario::where("folio",$request->id)->get();
        return response()->json(['data'=>$resultado]);
    }

    public function index(){

        return view('user.index');
    }

    public function sedesol(){

        return view('user.sedesol');
    }

    public function dondeEstamos(){

        return view('user.dondeEstamos');
    }

    public function programasSociales(){

        return view('user.programasSociales');
    }

    public function beneficios(){

        return view('user.beneficio');
    }

    public function tramites(){

        return view('user.tramites');
    }

    public function quejasDenuncias(){

        return view('user.quejas');
    }

    public function noticias(){

        return view('user.noticia');
    }

    public function contacto(){

        return view('user.contacto');
    }

}