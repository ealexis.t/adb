<?php

namespace App\Http\Controllers;

//use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Referencia;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function index()
    {

        /*
        echo "<html><head><meta charset=\"UTF-8\"><meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\" /></head><body>";
    header("Content-Type: text/html;charset=utf-8");
    $fila = 1;
    if (($gestor = fopen("liconsa_formato.csv", "r")) !== FALSE) {
        while (($datos = fgetcsv($gestor, 1000, ",")) !== FALSE) {
            $numero = count($datos);
            $fila++;
                if($datos[1]=="AGUASCALIENTES")
                {
                 $datos[1]="1";
                }
                else if($datos[1]=="BAJA CALIFORNIA")
                {
                 $datos[1]="2";
                }
                else if($datos[1]=="BAJA CALIFORNIA SUR")
                {
                 $datos[1]="3";
                }
                else if($datos[1]=="CAMPECHE")
                {
                 $datos[1]="4";
                }
                else if($datos[1]=="CHIAPAS")
                {
                 $datos[1]="5";
                }
                else if($datos[1]=="CHIHUAHUA")
                {
                 $datos[1]="6";
                }
                else if($datos[1]=="COAHUILA DE ZARAGOZA" || $datos[1]=="COAHUILA")
                {
                 $datos[1]="7";
                }
                else if($datos[1]=="COLIMA")
                {
                 $datos[1]="8";
                }
                 else if($datos[1]=="DISTRITO FEDERAL")
                {
                 $datos[1]="9";
                }
                 else if($datos[1]=="DURANGO")
                {
                 $datos[1]="10";
                }
                else if($datos[1]=="GUANAJUATO")
                {
                 $datos[1]="11";
                }
                  else if($datos[1]=="GUERRERO")
                {
                 $datos[1]="12";
                }
                                 else if($datos[1]=="HIDALGO")
                {
                 $datos[1]="13";
                }
                                 else if($datos[1]=="JALISCO")
                {
                 $datos[1]="14";
                }
                                 else if($datos[1]=="MEXICO" || $datos[1]=="EDO. DE MEXICO" || $datos[1]=="EDO DE MEXICO")
                {
                 $datos[1]="15";
                }
                                 else if($datos[1]=="MICHOACAN DE OCAMPO" || $datos[1]=="MICHOACAN")
                {
                 $datos[1]="16";
                }
                                 else if($datos[1]=="MORELOS")
                {
                 $datos[1]="17";
                }
                                 else if($datos[1]=="NAYARIT")
                {
                 $datos[1]="18";
                }
                                 else if($datos[1]=="NUEVO LEON")
                {
                 $datos[1]="19";
                }
                                 else if($datos[1]=="OAXACA")
                {
                 $datos[1]="20";
                }
                                 else if($datos[1]=="PUEBLA")
                {
                 $datos[1]="21";
                }
                                 else if($datos[1]=="QUERETARO")
                {
                 $datos[1]="22";
                }
                                 else if($datos[1]=="QUINTANA ROO")
                {
                 $datos[1]="23";
                }
                                 else if($datos[1]=="SAN LUIS POTOSI")
                {
                 $datos[1]="24";
                }
                                 else if($datos[1]=="SINALOA")
                {
                 $datos[1]="25";
                }
                                 else if($datos[1]=="SONORA")
                {
                 $datos[1]="26";
                }
                                 else if($datos[1]=="TABASCO")
                {
                 $datos[1]="27";
                }
                                 else if($datos[1]=="TAMAULIPAS")
                {
                 $datos[1]="28";
                }
                                 else if($datos[1]=="TLAXCALA")
                {
                 $datos[1]="29";
                }
                                 else if($datos[1]=="VERACRUZ DE IGNACIO DE LA LLAVE" || $datos[1]=="VERACRUZ")
                {
                 $datos[1]="30";
                }
                                 else if($datos[1]=="YUCATAN")
                {
                 $datos[1]="31";
                }
                else if($datos[1]=="ZACATECAS")
                {
                 $datos[1]="32";
                }
                $referencia= new Referencia;
                $referencia->institucion_id=$datos[0];
                $referencia->estado=$datos[1];
                $referencia->direccion=utf8_encode($datos[2]);
                $referencia->latitud=$datos[3];
                $referencia->longitud=$datos[4];
                $referencia->responsable=utf8_encode($datos[5]);
                $referencia->horario=utf8_encode($datos[6]);
                $referencia->telefonos=$datos[7];
                $referencia->revision=$datos[8];
                $referencia->save();
                echo utf8_encode(implode(" ",$datos))."</br>";

        }
        fclose($gestor);

    }
            echo "</body></html>";
    */

        return redirect()->to('user/home');

    }
}