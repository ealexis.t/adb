<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');


//Route::get('enlace', 'EnlaceQuejaController@create');

/*
|--------------------------------------------------------------------------
| Application Routes
|-------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();

/*
|--------------------------------------------------------------------------
| AdminController Routes
|-------------------------------------------------------------------
*/
    Route::get('admin/dashboard', 'AdminController@dashboard');
    Route::get('admin/users', 'AdminController@users');
});

Route::get('home', 'HomeController@index');
/*
|--------------------------------------------------------------------------
| UserController Routes
|-------------------------------------------------------------------
*/
    Route::get('user/home', 'UserController@index');
    Route::get('Sedesol', 'UserController@sedesol');
    Route::get('Donde-Estamos', 'UserController@dondeEstamos');
    Route::get('Programas-Sociales', 'UserController@programasSociales');
    Route::get('Beneficios', 'UserController@beneficios');
    Route::get('Tramites', 'UserController@tramites');
    Route::get('Quejas-Denuncias', 'UserController@quejasDenuncias');
    Route::get('Noticias', 'UserController@noticias');
    Route::get('Contacto', 'UserController@contacto');
/*
|--------------------------------------------------------------------------
| UserController Routes
|-------------------------------------------------------------------
*/

Route::get('Programas-Sociales/conadis', 'ProgramasController@conadis');
Route::get('Programas-Sociales/imjuve', 'ProgramasController@imjuve');
Route::get('Programas-Sociales/inaes', 'ProgramasController@inaes');
Route::get('Programas-Sociales/fonart', 'ProgramasController@fonart');
Route::get('Programas-Sociales/diconsa', 'ProgramasController@diconsa');
Route::get('Programas-Sociales/liconsa', 'ProgramasController@liconsa');
Route::get('Programas-Sociales/prospera', 'ProgramasController@prospera');
Route::get('Programas-Sociales/indesol', 'ProgramasController@indesol');
Route::get('Programas-Sociales/inapam', 'ProgramasController@inapam');
Route::get('Programas-Sociales/pei', 'ProgramasController@pei');
Route::get('Programas-Sociales/pfes', 'ProgramasController@dgpop');
Route::get('Programas-Sociales/svjf', 'ProgramasController@svjf');
Route::get('Programas-Sociales/ppam', 'ProgramasController@ppam');
Route::get('Programas-Sociales/paja', 'ProgramasController@paja');
Route::get('Programas-Sociales/pet', 'ProgramasController@pet');
Route::get('Programas-Sociales/tres', 'ProgramasController@tres');
Route::get('Programas-Sociales/comedores', 'ProgramasController@comedores');
Route::get('Programas-Sociales/coinversion', 'ProgramasController@coinversion');
Route::get('Programas-Sociales/paimef', 'ProgramasController@paimef');

Route::get('Donde-Estamos/fonart', 'UbicaController@tiendasfonart');
Route::get('Donde-Estamos/diconsa', 'UbicaController@tiendasdiconsa');
Route::get('Donde-Estamos/pei', 'UbicaController@pei');
Route::get('Donde-Estamos/delegaciones', 'UbicaController@delegaciones');
Route::get('Donde-Estamos/imjuve', 'UbicaController@imjuve');
Route::get('Donde-Estamos/liconsa', 'UbicaController@liconsa');
Route::get('Donde-Estamos/inapam', 'UbicaController@inapam');
Route::get('Donde-Estamos/organizaciones', 'UbicaController@organizaciones');
Route::get('Donde-Estamos/prospera', 'UbicaController@prospera');
Route::get('Donde-Estamos/paimef', 'UbicaController@paimef');
Route::get('Donde-Estamos/comedores', 'UbicaController@comedores');
Route::get('Donde-Estamos/casas', 'UbicaController@casas');


Route::get('actualiza', 'UbicaController@actualiza');
Route::get('carga', 'UbicaController@carga');
Route::get('descarga', 'UbicaController@descarga');
Route::get('gsp','UbicaController@gsp');
Route::get('getdata','UbicaController@getdata');
Route::get('getfolio','UserController@getfolio');

/*
|--------------------------------------------------------------------------
| BeneficiosController Routes
|-------------------------------------------------------------------
*/
Route::any('Beneficios/respuesta', 'BeneficiosController@respuesta');

Route::any('pdf', 'PdfController@invoice');
