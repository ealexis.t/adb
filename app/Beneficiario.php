<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beneficiario extends Model
{
    //Use table own
    protected $table = 'beneficiarios';

    protected $fillable = [
    	'id',
    	'folio',
        'nombre',
        'telefono',
        'celular',
        'email',
        'cp',
        'direccion',
    ];
}